// ExcelIncludes.h: header files necessary for Excel plugin.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_EXCELINCLUDES_H
#define DOME_EXCELINCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "StdAfx.h"
//#include "excel9.h"

//#define XL_2000

#include "DomePlugin.h" // includes string, vector, cout/endl, CheckPoint, DomeException
using namespace DOME::DomePlugin;


#include "TypeConversions.h" // str
using namespace DOME::Utilities::TypeConversions;

#define EXCEL_DEBUG(msg) cout << "EXCEL DEBUG: " << msg << endl;

#define EXCEL_ERROR1(msg) throw DomeException("Excel Error",msg)
#define EXCEL_ERROR2(msg)	\
	{						\
	  cout << msg << endl;	\
      throw DomeException("Excel Error",__FILE__,__LINE__,msg);	\
	}
						  
#define EXCEL_ERROR EXCEL_ERROR2

#ifdef _EXCEL_2KXP
#include "excel9.h"
#define XL_2000
#endif

#ifdef _EXCEL_97
#include "excel8.h"
#endif

#endif // DOME_EXCELINCLUDES_H
