#include "UgData.h"

namespace DOME {
namespace UgPlugin 
{

/*************************************************************************************
 *
 *						UgComponent
 *
 ************************************************************************************/

UgComponent::UgComponent(string part)
{
	compName.assign(part.c_str());
    topLevel = false;
	exists = false;
	compPtr = NULL;
	analysis_unit = 1;
	haveChangedDimIn = 0;
	isAssembly = false;
}

UgComponent::UgComponent(string part, string analysis)
{
	compName.assign(part.c_str());
    topLevel = false;
	exists = false;
	compPtr = NULL;
	parent = 0;
	if (!analysis.compare("lb in"))
		analysis_unit = 1;
	else
	{
		if (!analysis.compare("lb ft"))
			analysis_unit = 2;
		else
		{
			if (!analysis.compare("g cm"))
				analysis_unit = 3;
			else
				if (!analysis.compare("kg m"))
					analysis_unit = 4;
				else
					analysis_unit = 1; // default
		}
	}
	haveChangedDimIn = 0;
	isAssembly = false;
}

UgMassProperty* UgComponent::createMassProperty(string propertyName)
{
    UgMassProperty * iMass = new UgMassProperty(propertyName);
    massProperties.push_back(iMass);
    return iMass;
}

UgExportFile* UgComponent::createExportFile(string expName)
{
    UgExportFile * iFile = new UgExportFile(expName);
    exports.push_back(iFile);
    return iFile;
}

UgDimensionIn* UgComponent::createDimensionIn(string dimName)
{
    UgDimensionIn * iDim = new UgDimensionIn(dimName);
	iDim->setExpDOMEin(NULL);
	iDim->setNumSegments(0);
	iDim->setHaveSegments(false);
    dimensionsIn.push_back(iDim);
    return iDim;
}

UgDimensionOut* UgComponent::createDimensionOut(string dimName)
{
    UgDimensionOut * iDim = new UgDimensionOut(dimName);
	iDim->setExpDOMEout(NULL);
	iDim->setNumSegments(0);
	iDim->setHaveSegments(false);
    dimensionsOut.push_back(iDim);
    return iDim;
}

UgDimensionBool* UgComponent::createDimensionBool(string dimName)
{
    UgDimensionBool * iDim = new UgDimensionBool(dimName);
	iDim->setExpDOMEin(NULL);
    dimensionsBool.push_back(iDim);
    return iDim;
}

void UgComponent::prepPart()
{
	int	body_type, errorCode, subtype, type, first_time = 1, i, j;
	char	buf[MAX_LN_SIZE+1];
	tag_t	member;
	UgTypedObject	*pCurObj;
	double bounding_box[6], bounding_box2[6];

	try
	{
		UgSession::setDisplayPart( compPtr );		
		UgSession::setWorkPart( compPtr );		
		if (compPtr->isMillimeters())
			dimUnit.assign( string("millimeter") );
		else 
			dimUnit.assign( string("inch") );
		for (pCurObj = compPtr->iterateFirst(); pCurObj; pCurObj = compPtr->iterateNext(pCurObj))
		{
			member = pCurObj->getTag();
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
			// obtain bounding box limits
			if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
			{
				errorCode = UF_MODL_ask_body_type( member, &body_type );
				if ( body_type == UF_MODL_SOLID_BODY )
				{
					errorCode = UF_MODL_ask_bounding_box( member, bounding_box );
					if (first_time == 1)
					{
						for (j = 0; j < 6; j++)
							bounding_box2[j] = bounding_box[j];
						first_time = 0;
					}
					else
					{
						for (j = 0; j < 3; j++) // obtain min
						{
							if (bounding_box[j] < bounding_box2[j])
								bounding_box2[j] = bounding_box[j];
						}
						for (j = 3; j < 6; j++) // obtain max
						{
							if (bounding_box[j] > bounding_box2[j])
								bounding_box2[j] = bounding_box[j];
						}
					}
				}
			}
			if ( type == UF_solid_type && subtype == UF_solid_body_subtype )
			{
				errorCode = UF_MODL_ask_body_type( member, &body_type );
				if ( body_type == UF_MODL_SOLID_BODY )
				{
					sprintf(buf, "%s", compName.c_str());
					errorCode = UF_OBJ_set_name( member, buf );
				}
			}

		}
		// set components's boundingBox
		for ( i = 0; i < 6; i++ ) boundingBox.push_back(bounding_box2[i]);
	}
	catch (...)
	{
		UG_ERROR("UgComponent::prepPart: unknown exception caught");
		clean();
	}
}

void UgComponent::updateBoundingBoxForAss()
{
	int	body_type, errorCode, subtype, type, first_time = 1, i, j;
	char	buf[MAX_LN_SIZE+1];
	tag_t	member;
	UgTypedObject	*pCurObj;
	double bounding_box[6], bounding_box2[6], bounding_box3[6], transform[4][4];
	
	// this function must be invoked for the top assembly ONLY (for now) every time a new VRML file
	// is to be exported; need the bounding box to envelope the DOME components completely
	try
	{
		if (!topLevel) // if is top level, would not need to update info already captured elsewhere
		{
			for (pCurObj = compPtr->iterateFirst(); pCurObj; pCurObj = compPtr->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				if(errorCode)
				{
					UG_ERROR("...exiting application");
					return;
				}
				// obtain bounding box limits
				if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
				{
					errorCode = UF_MODL_ask_body_type( member, &body_type );
					if(errorCode)
					{
						UG_ERROR("...exiting application");
						return;
					}
					if ( body_type == UF_MODL_SOLID_BODY )
					{
						errorCode = UF_MODL_ask_bounding_box( member, bounding_box );
						if(errorCode)
						{
							UG_ERROR("...exiting application");
							return;
						}
						if (first_time == 1)
						{
							for (j = 0; j < 6; j++)
								bounding_box2[j] = bounding_box[j];
							first_time = 0;
						}
						else
						{
							for (j = 0; j < 3; j++) // obtain min
							{
								if (bounding_box[j] < bounding_box2[j])
									bounding_box2[j] = bounding_box[j];
							}
							for (j = 3; j < 6; j++) // obtain max
							{
								if (bounding_box[j] > bounding_box2[j])
									bounding_box2[j] = bounding_box[j];
							}
						}
					}
				}
			}
			// take transform of occurrence into consideration
			errorCode = UF_ASSEM_ask_transform_of_occ(instanceTag, transform);
			if(errorCode)
			{
				UG_ERROR("UF_ASSEMB_ask_transform_of_occ");
				return;
			}
			UG_DEBUG("transform matrix");
			UG_DEBUG("print this");
			sprintf(buf, "%f %f %f %f", transform[0][0], transform[0][1], transform[0][2], transform[0][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[1][0], transform[1][1], transform[1][2], transform[1][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[2][0], transform[2][1], transform[2][2], transform[2][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[3][0], transform[3][1], transform[3][2], transform[3][3]);
			UG_DEBUG(buf);

			bounding_box3[0] = transform[0][0]*bounding_box2[0] + transform[0][1]*bounding_box2[1] +
				transform[0][2]*bounding_box2[2] + transform[0][3];

			bounding_box3[1] = transform[1][0]*bounding_box2[0] + transform[1][1]*bounding_box2[1] +
				transform[1][2]*bounding_box2[2] + transform[1][3];

			bounding_box3[2] = transform[2][0]*bounding_box2[0] + transform[2][1]*bounding_box2[1] +
				transform[2][2]*bounding_box2[2] + transform[2][3];

			bounding_box3[3] = transform[0][0]*bounding_box2[3] + transform[0][1]*bounding_box2[4] +
				transform[0][2]*bounding_box2[5] + transform[0][3];

			bounding_box3[4] = transform[1][0]*bounding_box2[3] + transform[1][1]*bounding_box2[4] +
				transform[1][2]*bounding_box2[5] + transform[1][3];

			bounding_box3[5] = transform[2][0]*bounding_box2[3] + transform[2][1]*bounding_box2[4] +
				transform[2][2]*bounding_box2[5] + transform[2][3];

			// set components's boundingBox
			if (!boundingBoxAssCoord.empty())
				boundingBoxAssCoord.pop_back();
			for ( i = 0; i < 6; i++ )
				boundingBoxAssCoord.push_back(bounding_box3[i]);
		}
	}
	catch (...)
	{
		UG_ERROR("UgComponent::updateBoundingBoxForAss: unknown exception caught");
		clean();
	}
}

void UgComponent::findDimObjects()
{
	int	i, found, numIn=0, numOut=0, numBool=0;
	string name; 

	try
	{
		for (UgIterator<UgExpression> it; !it.isFinished(); it.findNext())
		{
			/*
			 *
			 *	This is where I am getting a major pointer problem 
			 *  and my code crashes all the time
			 *
			 */

			name = (*it)->getLHS();
			found = -1;
			if ( numIn < dimensionsIn.size() )
			{

				for ( i = 0; i < dimensionsIn.size(); i++ )
				{
					if ( !name.compare(dimensionsIn[i]->getName()) )
					{
						dimensionsIn[i]->setExists(true);
						dimensionsIn[i]->setExpDOMEin(*it); 
						dimensionsIn[i]->setValueInit( (*it)->askValue() ); 
						dimensionsIn[i]->setUnit( dimUnit ); 
						if ( !isAssembly )
							dimensionsIn[i]->findSegments(compPtr);
						found = 1;
						++numIn;
						break;
					}
				}
			}
			if ( found < 0 )
			{
				if ( numOut < dimensionsOut.size() )
				{
					for ( i = 0; i < dimensionsOut.size(); i++ )
					{
						if ( !name.compare(dimensionsOut[i]->getName()) )
						{
							dimensionsOut[i]->setExists(true);
							dimensionsOut[i]->setExpDOMEout(*it);
							dimensionsOut[i]->setValue( (*it)->askValue() );
							dimensionsOut[i]->setUnit( dimUnit );
							if ( !isAssembly )
								dimensionsOut[i]->findSegments(compPtr);
							found = 1;
							++numOut;
							break;
						}
					}
				}
				if ( found < 0 && numBool < dimensionsBool.size() )
				{
					for ( i = 0; i < dimensionsBool.size(); i++ )
					{
						if ( !name.compare(dimensionsBool[i]->getName()) )
						{
							dimensionsBool[i]->setExists(true);
							dimensionsBool[i]->setExpDOMEin(*it);
							dimensionsBool[i]->setValueBool( (*it)->askValue() );
							dimensionsBool[i]->setUnit( string("unitless") );
							++numBool;
							break;
						}
					}
				}
			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgComponent::findDimObjects: unknown exception caught");
		clean();
	}
}

void UgComponent::findAllMassValues()
{
	char	buf[MAX_LN_SIZE+1];
	double  acc_val[11] = {.9,0,0,0,0,0,0,0,0,0,0}; // has no effect in my case
													 // since used for definable solids
	double  density = 1.0; // this value has no significance in solid body analysis;
						   // desired density is already set in the part files
	double	massprop[47], massprop_stat[13];
	int	accuracy=1, i, errorCode, type_a=1; // type_a = 1 for solid bodies
	int select[40] = {46,1,0,2,6,7,8,3,4,5,9,10,11,12,13,14,16,17,18,19,20,21,34,35,36,37,38,39,31,32,33,
		22,23,24,25,26,27,28,29,30};
	tag_t	solbody=NULL_TAG;
	vector	<tag_t> solbody2;

	try
	{
		sprintf(buf, "%s", compName.c_str());

		while ( !(errorCode = UF_OBJ_cycle_by_name(buf,	&solbody)) && (solbody != NULL_TAG))
			solbody2.push_back( solbody );

		solbody2.push_back( NULL_TAG );

		tag_t	*solbody3=new tag_t[solbody2.size()];

		for (i = 0; i < solbody2.size(); i++ )
			solbody3[i]=solbody2[i];

		errorCode = UF_MODL_ask_mass_props_3d(solbody3, solbody2.size()-1, type_a, 
			analysis_unit, density, accuracy, acc_val, massprop, massprop_stat);

		for ( i = 0; i < massProperties.size(); i++ )
		{
			massProperties[i]->setValue(massprop[select[massProperties[i]->getType()]]);
		}
		delete []solbody3;
	}
	catch (...)
	{
		UG_ERROR("UgComponent::findAllMassValues: unknown exception caught");
		clean();
	}
}

void UgComponent::findMassValues(vector <string> MassName)
{
	int i;

	try
	{
		for ( i = 0; i < massProperties.size(); i++ )
		{
			massProperties[i]->findType(MassName);
			massProperties[i]->setUnit( findDOMEoutUnit(massProperties[i]->getType()) );
		}
		if ( massProperties.size() > 0 )
			findAllMassValues();
	}
	catch (...)
	{
		UG_ERROR("UgComponent::findMassValues: unknown exception caught");
		clean();
	}
}

string UgComponent::findDOMEoutUnit( int type )
{
    string  unit_text[] = {"pounds mass per cubic inch","pounds mass per cubic foot", 
              "grams per cubic centimeter", "kilograms per cubic meter", 
              "square inch","square foot","square centimeter","square meter", 
              "cubic inch","cubic foot","cubic centimeter","cubic meter","pound mass",
			  "gram","kilogram","pound mass inch","pound mass foot","gram centimeter",
			  "kilogram meter","inch","feet","centimeter","meter","pound mass square inch",
			  "pound mass square foot","gram square centimeter","kilogram square meter",
			  "unitless"}; 
	string  output_unit;

	switch (type)
	{
		case 0: 
			if (analysis_unit == 1)
				output_unit=unit_text[0];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[1];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[2];
					else
						output_unit=unit_text[3];
				}
			}
			break;
		case 1: 
			if (analysis_unit == 1)
				output_unit=unit_text[8];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[9];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[10];
					else
						output_unit=unit_text[11];
				}
			}
			break;
		case 2:
			if (analysis_unit == 1)
				output_unit=unit_text[4];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[5];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[6];
					else
						output_unit=unit_text[7];
				}
			}
			break;
		case 3: 
			if (analysis_unit == 1)
				output_unit=unit_text[12];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[12];
				else
				{
					if (analysis_unit == 3)
							output_unit=unit_text[13];
					else
						output_unit=unit_text[14];
				}
			}
			break;
		case 4: case 5: case 6:
			if (analysis_unit == 1)
				output_unit=unit_text[15];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[16];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[17];
					else
						output_unit=unit_text[18];
				}
			}
			break;
		case 7: case 8: case 9: case 22: case 23: case 24: case 25: case 26: case 27:
			if (analysis_unit == 1)
				output_unit=unit_text[19];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[20];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[21];
					else
						output_unit=unit_text[22];
				}
			}
			break;
		case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 18: case 19: case 20: case 21:
		case 28: case 29: case 30:
			if (analysis_unit == 1)
				output_unit=unit_text[23];
			else
			{
				if (analysis_unit == 2)
					output_unit=unit_text[24];
				else
				{
					if (analysis_unit == 3)
						output_unit=unit_text[25];
					else
						output_unit=unit_text[26];
				}
			}
			break;
		case 31: case 32: case 33: case 34: case 35: case 36: case 37: case 38: case 39:
			output_unit=unit_text[27];
			break;
	}
	return output_unit;
}

void UgComponent::updateDimInValues()
{
	int i;

	UG_DEBUG("in UgComponent::updateDimInValues");

	UgSession::setWorkPart( compPtr );
	
	for ( i = 0; i < dimensionsIn.size(); i++ )
	{
		if ( dimensionsIn[i]->getChanged() )
		{
			dimensionsIn[i]->updateValue();
			++haveChangedDimIn;
		}
	}
	for ( i = 0; i < dimensionsBool.size(); i++ )
	{
		if ( dimensionsBool[i]->getChanged() )
		{
			dimensionsBool[i]->updateValue();
			++haveChangedDimIn;
		}
	}
}


void UgComponent::updateOutputValues()
{
	int i;

	UG_DEBUG("in UgComponent::updateOutputValues");

	UgSession::setWorkPart( compPtr );

	if ( haveChangedDimIn > 0 )
	{
        i = UF_MODL_update();
		for ( i = 0; i < dimensionsOut.size(); i++ )
			dimensionsOut[i]->updateValue();
	}
	findAllMassValues();
	haveChangedDimIn = 0;
}

void UgComponent::findExportInfo(string dirName)
{
	int i;
	char buf[256];
	string in;

	UG_DEBUG("in UgComponent::findExportInfo");

	for ( i = 0; i < exports.size(); i++ )
	{
		exports[i]->findExportType();
		switch(exports[i]->getType())
		{
			case 1:
				sprintf(buf, "%s\\export\\i_%s.igs", dirName.c_str(), compName.c_str());
				in.assign(buf);
				exports[i]->setFileName(in);
				break;
			case 2:
				sprintf(buf, "%s\\export\\s_%s.stp", dirName.c_str(), compName.c_str());
				in.assign(buf);
				exports[i]->setFileName(in);
				break;
		}
	}
}

void UgComponent::exportAll(string dirName, string export_directory, string UGII_BASE_DIR)
{
	char	buf[MAX_LN_SIZE+1];
	int i;
	string	base_dir, in, out, ug_to_iges, ug_to_step, log;

	UG_DEBUG("in UgComponent::exportAll");

	try
	{
		for ( i = 0; i < exports.size(); i ++ )
		{
			sprintf(buf, "export translator type: %d", exports[i]->getType());
			UG_DEBUG(buf);
			in.assign(dirName);
			in.append("\\");
			in.append(compName);
			in.append(".prt");
			// export options: 0->none; 1->.igs; 2->203.stp, latest is 214.stp though
			switch (exports[i]->getType())
			{
				case 1:
					sprintf(buf,"o=%s", exports[i]->getFileName().c_str());
					out = string( buf );
					sprintf(buf, "out to %s", out.c_str());
					UG_DEBUG(buf); 
					sprintf(buf, "%s\\IGES\\iges.cmd", UGII_BASE_DIR.c_str());
					base_dir = string( buf );
					sprintf(buf, "d=%s\\IGES\\igesexport.def", UGII_BASE_DIR.c_str());
					ug_to_iges = string( buf );
					sprintf(buf, "l=%s\\%s_ugIges.log", export_directory.c_str(), compName.c_str());
					log = string( buf );
					sprintf(buf,"%s %s %s %s %s", base_dir.c_str(), in.c_str(), out.c_str(), log.c_str(),
						ug_to_iges.c_str());
					system(buf); 
					break;
				case 2:
					sprintf(buf,"o=%s",exports[i]->getFileName().c_str());
					out = string( buf );
					sprintf(buf, "out to %s", out.c_str());
					UG_DEBUG(buf); 
/*					sprintf(buf, "%s\\STEP203UG\\step203ug.cmd", UGII_BASE_DIR.c_str());
					base_dir = UgString( buf );
					sprintf(buf, "d=%s\\STEP203UG\\ugstep203.def", UGII_BASE_DIR.c_str());
					ug_to_step = UgString( buf );*/
					sprintf(buf, "%s\\STEP214UG\\step214ug.cmd", UGII_BASE_DIR.c_str());
					base_dir = string( buf );
					sprintf(buf, "d=%s\\STEP214UG\\ugstep214.def", UGII_BASE_DIR.c_str());
					ug_to_step = string( buf );
					sprintf(buf, "l=%s\\%s_ugStep.log", export_directory.c_str(), compName.c_str());
					log = string( buf );
					sprintf(buf,"%s %s %s %s %s", base_dir.c_str(), in.c_str(), out.c_str(), log.c_str(),
						ug_to_step.c_str());
					system(buf); 
					break;
			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgComponent::exportAll: unknown exception caught");
		clean();
	}
}

void UgComponent::clean()
{
	int i ;

	topLevel = false;
	exists = false;
	analysis_unit = 1;
	compPtr = NULL;
	parent = 0;
	for ( i = 0; i < dimensionsIn.size(); i++ )
		dimensionsIn[i]->clean();
	for ( i = 0; i < dimensionsOut.size(); i++ )
		dimensionsOut[i]->clean();
	for ( i = 0; i < dimensionsBool.size(); i++ )
		dimensionsBool[i]->clean();
	for ( i = 0; i < massProperties.size(); i++ )
		massProperties[i]->clean();
	for ( i = 0; i < exports.size(); i++ )
		exports[i]->clean();
	while (!boundingBox.empty())
		boundingBox.pop_back();
}

void UgComponent::cleanAll()
{
	clean();
	while (!dimensionsIn.empty())
		dimensionsIn.pop_back();
	while (!dimensionsOut.empty())
		dimensionsOut.pop_back();
	while (!dimensionsBool.empty())
		dimensionsBool.pop_back();
	while (!massProperties.empty())
		massProperties.pop_back();
	while (!exports.empty())
		exports.pop_back();
}

void UgComponent::printToVRML(string wrl, string export_directory, vector<string> allCompExists)
{
	char buf[MAX_LN_SIZE+1], line[MAX_LN_SIZE+1];
	float numHi, numLow;
	int	i, ii, j, jj, k;
	FILE *fp, *fp2;

	try
	{
		ii = -1;
		if ( !isAssembly )
		{
			fp = fopen(wrl.c_str(), "a");
			for ( i = 0; i < dimensionsIn.size(); i++ )
			{
				if ( dimensionsIn[i]->getExists() )
				{
					++ii;
					if ( dimensionsIn[i]->getHaveSegments() )
					{
						sprintf(buf, "\nDEF SIn%d Script {\n", ii );
						fputs( buf, fp );
						sprintf(buf, "    eventIn SFBool highlight eventIn SFInt32 turnOff eventIn SFFloat listSelect\n");
						fputs( buf, fp );
						sprintf(buf, "    eventOut SFColor colorTo eventOut SFInt32 index eventOut SFBool wasTurnedOn\n");
						fputs( buf, fp );
						sprintf(buf, "    field SFColor colorHighlight 1 1 0 field SFColor colorOriginal 1 1 1\n\n");
						fputs( buf, fp );
						sprintf(buf, "    url \"javascript:\n");
						fputs( buf, fp );
						sprintf(buf, "    function initialize() { index = -1; colorTo = colorOriginal; wasTurnedOn = false; }\n");
						fputs( buf, fp );
						sprintf(buf, "    function highlight(active) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (active)\n");
						fputs( buf, fp );
						sprintf(buf, "        { colorTo = colorHighlight; index = %d; wasTurnedOn = true; }\n    }\n", ii);
						fputs( buf, fp );
						sprintf(buf, "    function turnOff(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (value != %d && wasTurnedOn)\n", ii);
						fputs( buf, fp );
						sprintf(buf, "        { colorTo = colorOriginal; wasTurnedOn = false; }\n    }\n");
						fputs( buf, fp );
						sprintf(buf, "    function listSelect(value) {\n");
						fputs( buf, fp );
						numHi = ii + 0.1;	numLow = ii - 0.1;
						sprintf(buf, "        if ((value < %.1f) && (value > %.1f) && !wasTurnedOn)\n", numHi, numLow);
						fputs( buf, fp );
						sprintf(buf, "        { colorTo = colorHighlight; index = %d; wasTurnedOn = true; }\n", ii);
						fputs( buf, fp );
						sprintf(buf, "        else\n        {\n");
						fputs( buf, fp );
						sprintf(buf, "          if ((value < -0.9) && (value > -1.1))\n");
						fputs( buf, fp );
						sprintf(buf, "            index = -1;\n        }\n    }\"\n}\n");
						fputs( buf, fp );

						sprintf(buf, "\nGroup { children [\n" );
						fputs( buf, fp );
						sprintf(buf, "    Transform { children [\n" );
						fputs( buf, fp );
						sprintf(buf, "        DEF Tin%d TouchSensor {}\n", ii);
						fputs( buf, fp );
						sprintf(buf, "        Shape {\n" );
						fputs( buf, fp );
						sprintf(buf, "            appearance Appearance { material DEF Min%da Material {\n", ii);
						fputs( buf, fp );
						sprintf(buf, "                diffuseColor 1 1 1 emissiveColor 1 1 1 transparency 0.0 } }\n" );
						fputs( buf, fp );
						sprintf(buf, "            geometry IndexedLineSet {\n" );
						fputs( buf, fp );
						sprintf(buf, "                coord Coordinate { point [ %.4f %.4f %.4f, %.4f %.4f %.4f\n", 
							dimensionsIn[i]->getStartX(0), dimensionsIn[i]->getStartY(0), dimensionsIn[i]->getStartZ(0),
							dimensionsIn[i]->getEndX(0), dimensionsIn[i]->getEndY(0), dimensionsIn[i]->getEndZ(0) );
						fputs( buf, fp );
						for ( j = 1; j < dimensionsIn[i]->getNumSegments(); j++ )
						{
							sprintf(buf, "                ,%.4f %.4f %.4f, %.4f %.4f %.4f\n", dimensionsIn[i]->getStartX(j),
								dimensionsIn[i]->getStartY(j), dimensionsIn[i]->getStartZ(j),
								dimensionsIn[i]->getEndX(j), dimensionsIn[i]->getEndY(j), dimensionsIn[i]->getEndZ(j));
							fputs( buf, fp );
						}
						sprintf(buf, "                ] }\n" );
						fputs( buf, fp );
						k = 0;
						sprintf(buf, "                coordIndex [ " );
						fputs( buf, fp );
						for ( j = 0; j < dimensionsIn[i]->getNumSegments(); j++ )
						{
							sprintf(buf, "%d %d -1 ", k, k+1 );
							fputs( buf, fp );
							k+=2;
						}
						sprintf(buf, " ] } }\n" );
						fputs( buf, fp );
						sprintf(buf, "        Transform {\n" );
						fputs( buf, fp );
						sprintf(buf, "           translation %.4f %.4f %.4f\n", dimensionsIn[i]->getOriginX(),
							dimensionsIn[i]->getOriginY(), dimensionsIn[i]->getOriginZ());
						fputs( buf, fp );
						sprintf(buf, "           children [ Billboard { axisOfRotation 0 0 0\n" );
						fputs( buf, fp );
						sprintf(buf, "               children [ Shape {\n" );
						fputs( buf, fp );
						sprintf(buf, "                   appearance Appearance { material DEF Min%db Material {\n", ii); 
						fputs( buf, fp );
						sprintf(buf, "                       diffuseColor 1 1 1 emissiveColor 1 1 1 transparency 0.0 } }\n"); 
						fputs( buf, fp );
						sprintf(buf, "                   geometry DEF TxtIn%d Text { string [ \"%s=%.4f\" ] \n", ii,
							dimensionsIn[i]->getName().c_str(), dimensionsIn[i]->getValue() );
						fputs( buf, fp );
						sprintf(buf, "                       fontStyle FontStyle { size %.4f } }\n",
							dimensionsIn[i]->getHeight());
						fputs( buf, fp );
						sprintf(buf, "} ] } ] } ] } ] }\n" );
						fputs( buf, fp );
					}
				}
			}

			ii = -1;
			for ( i = 0; i < dimensionsOut.size(); i++ )
			{
				if ( dimensionsOut[i]->getExists() )
				{
					++ii;
					if ( dimensionsOut[i]->getHaveSegments() )
					{
						sprintf(buf, "\nDEF SOut%d Script {\n", ii );
						fputs( buf, fp );
						sprintf(buf, "    eventIn SFBool highlight eventIn SFInt32 turnOff eventIn SFFloat listSelect\n");
						fputs( buf, fp );
						sprintf(buf, "    eventOut SFColor colorTo eventOut SFInt32 index eventOut SFBool wasTurnedOn\n");
						fputs( buf, fp );
						sprintf(buf, "    field SFColor colorHighlight 1 0 0 field SFColor colorOriginal 0 0.7 0.9\n\n");
						fputs( buf, fp );
						sprintf(buf, "    url \"javascript:\n");
						fputs( buf, fp );
						sprintf(buf, "    function initialize() { index = -1; colorTo = colorOriginal; wasTurnedOn = false; }\n");
						fputs( buf, fp );
						sprintf(buf, "    function highlight(active) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (active)\n");
						fputs( buf, fp );
						jj = -1*(ii + 2);
						sprintf(buf, "        { colorTo = colorHighlight; index = %d; wasTurnedOn = true; }\n    }\n", jj);
						fputs( buf, fp );
						sprintf(buf, "    function turnOff(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (value != %d && wasTurnedOn)\n", jj);
						fputs( buf, fp );
						sprintf(buf, "        { colorTo = colorOriginal; wasTurnedOn = false; }\n    }\n");
						fputs( buf, fp );
						sprintf(buf, "    function listSelect(value) {\n");
						fputs( buf, fp );
						numHi = jj + 0.1;	numLow = jj - 0.1;
						sprintf(buf, "        if ((value < %.1f) && (value > %.1f) && !wasTurnedOn)\n", numHi, numLow);
						fputs( buf, fp );
						sprintf(buf, "        { colorTo = colorHighlight; index = %d; wasTurnedOn = true; }\n", jj);
						fputs( buf, fp );
						sprintf(buf, "        else\n        {\n");
						fputs( buf, fp );
						sprintf(buf, "          if ((value < -0.9) && (value > -1.1))\n");
						fputs( buf, fp );
						sprintf(buf, "            index = -1;\n        }\n    }\"\n}\n");
						fputs( buf, fp );

						sprintf(buf, "\nGroup { children [\n" );
						fputs( buf, fp );
						sprintf(buf, "    Transform { children [\n" );
						fputs( buf, fp );
						sprintf(buf, "        DEF Tout%d TouchSensor {}\n", ii);
						fputs( buf, fp );
						sprintf(buf, "        Shape {\n" );
						fputs( buf, fp );
						sprintf(buf, "            appearance Appearance { material DEF Mout%da Material {\n", ii);
						fputs( buf, fp );
						sprintf(buf, "                diffuseColor 0 0.7 0.9 emissiveColor 0 0.7 0.9 transparency 0.0 } }\n" );
						fputs( buf, fp );
						sprintf(buf, "            geometry IndexedLineSet {\n" );
						fputs( buf, fp );
						sprintf(buf, "                coord Coordinate { point [ %.4f %.4f %.4f, %.4f %.4f %.4f\n", 
							dimensionsOut[i]->getStartX(0), dimensionsOut[i]->getStartY(0), dimensionsOut[i]->getStartZ(0),
							dimensionsOut[i]->getEndX(0), dimensionsOut[i]->getEndY(0), dimensionsOut[i]->getEndZ(0) );
							fputs( buf, fp );
							for ( j = 1; j < dimensionsOut[i]->getNumSegments(); j++ )
							{
								sprintf(buf, "                ,%.4f %.4f %.4f, %.4f %.4f %.4f\n", dimensionsOut[i]->getStartX(j),
									dimensionsOut[i]->getStartY(j), dimensionsOut[i]->getStartZ(j),
									dimensionsOut[i]->getEndX(j), dimensionsOut[i]->getEndY(j), dimensionsOut[i]->getEndZ(j));
								fputs( buf, fp );
							}
							sprintf(buf, "                ] }\n" );
							fputs( buf, fp );
							k = 0;
							sprintf(buf, "                coordIndex [ " );
							fputs( buf, fp );
							for ( j = 0; j < dimensionsOut[i]->getNumSegments(); j++ )
							{
							sprintf(buf, "%d %d -1 ", k, k+1 );
							fputs( buf, fp );
							k+=2;
						}
						sprintf(buf, " ] } }\n" );
						fputs( buf, fp );
						sprintf(buf, "        Transform {\n" );
						fputs( buf, fp );
						sprintf(buf, "           translation %.4f %.4f %.4f\n", dimensionsOut[i]->getOriginX(),
							dimensionsOut[i]->getOriginY(), dimensionsOut[i]->getOriginZ());
						fputs( buf, fp );
						sprintf(buf, "           children [ Billboard { axisOfRotation 0 0 0\n" );
						fputs( buf, fp );
						sprintf(buf, "               children [ Shape {\n" );
						fputs( buf, fp );
						sprintf(buf, "                   appearance Appearance { material DEF Mout%db Material {\n", ii); 
						fputs( buf, fp );
						sprintf(buf, "                       diffuseColor 0 0.7 0.9 emissiveColor 0 0.7 0.9 transparency 0.0 } }\n"); 
						fputs( buf, fp );
						sprintf(buf, "                   geometry DEF TxtOut%d Text { string [ \"%s=%.4f\" ] \n", ii,
							dimensionsOut[i]->getName().c_str(), dimensionsOut[i]->getValue() );
						fputs( buf, fp );
						sprintf(buf, "                       fontStyle FontStyle { size %.4f } }\n",
							dimensionsOut[i]->getHeight());
						fputs( buf, fp );
						sprintf(buf, "} ] } ] } ] } ] }\n" );
						fputs( buf, fp );
					}
				}
			}

			sprintf(buf, "\n] }\n" );
			fputs( buf, fp );

			ii = -1;
			for ( i = 0; i < dimensionsIn.size(); i++ )
			{
				if ( dimensionsIn[i]->getExists() )
				{
					++ii;
					if ( dimensionsIn[i]->getHaveSegments() )
					{
						sprintf(buf, "\nROUTE Tin%d.isActive TO SIn%d.highlight\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.shininess TO SIn%d.listSelect\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SIn%d.index TO RecordIn.record\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE RecordIn.store TO SIn%d.turnOff\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SIn%d.colorTo TO Min%da.set_diffuseColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SIn%d.colorTo TO Min%da.set_emissiveColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SIn%d.colorTo TO Min%db.set_diffuseColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SIn%d.colorTo TO Min%db.set_emissiveColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.transparency TO Min%da.set_transparency\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.transparency TO Min%db.set_transparency\n", ii);
						fputs( buf, fp );
					}
				}
			}

			ii = -1;
			for ( i = 0; i < dimensionsOut.size(); i++ )
			{
				if ( dimensionsOut[i]->getExists() )
				{
					++ii;
					if ( dimensionsOut[i]->getHaveSegments() )
					{
						sprintf(buf, "\nROUTE Tout%d.isActive TO SOut%d.highlight\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.shininess TO SOut%d.listSelect\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SOut%d.index TO RecordIn.record\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE RecordIn.store TO SOut%d.turnOff\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SOut%d.colorTo TO Mout%da.set_diffuseColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SOut%d.colorTo TO Mout%da.set_emissiveColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SOut%d.colorTo TO Mout%db.set_diffuseColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE SOut%d.colorTo TO Mout%db.set_emissiveColor\n", ii, ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.transparency TO Mout%da.set_transparency\n", ii);
						fputs( buf, fp );
						sprintf(buf, "ROUTE MatSet.transparency TO Mout%db.set_transparency\n", ii);
						fputs( buf, fp );
					}
				}
			}

			fclose(fp);
		}
		else
		{
			// add bounding boxes for top assembly only
			if (topLevel)
			{
				UG_DEBUG("yes here: about to copy over bounding box!!!");
				fp = fopen(wrl.c_str(), "a");
				UG_DEBUG("VRML file is: " << wrl.c_str());
//				sprintf(buf, "\n");
//				fputs( buf, fp );
				sprintf(buf, "%s\\bd", export_directory.c_str());
				fp2 = fopen(buf, "r");
				if ( fp2 != NULL )
				{
					while (	fgets(line, 500, fp2) )
						fputs( line, fp );
					fclose( fp2 );
				}
				fclose(fp);
			}

		}
		// now create file to be read by java
//		sprintf(buf, "%s\\%s.dat", export_directory.c_str(), compName.c_str());
		sprintf(buf, "%s\\%s.dat", export_directory.c_str(), topCompName.c_str());
		fp = fopen(buf, "w");
		if ( isAssembly )
			sprintf(buf, "0 \"%s\" -1 %d\n", compName.c_str(), compIndex);
		else
			sprintf(buf, "0 \"%s\" -2 %d\n", compName.c_str(), compIndex); // c for component
		fputs( buf, fp );
		for ( i = 0; i < dimensionsIn.size(); i++ )
		{
			if ( dimensionsIn[i]->getExists() )
			{

				if ( dimensionsIn[i]->getHaveSegments() )
				{
					sprintf(buf, "1 \"%s\" %.4f \"%s\" %d\n", dimensionsIn[i]->getName().c_str(), dimensionsIn[i]->getValue(),
						dimensionsIn[i]->getUnit().c_str(), i);
					fputs( buf, fp );
				}
				else
				{
					sprintf(buf, "2 \"%s\" %.4f \"%s\" %d\n", dimensionsIn[i]->getName().c_str(), dimensionsIn[i]->getValue(),
						dimensionsIn[i]->getUnit().c_str(), i);
					fputs( buf, fp );
				}
			}
		}
/*		for ( i = 0; i < dimensionsBool.size(); i++ )
		{
			if ( dimensionsBool[i]->getExists() )
			{
				if ( dimensionsBool[i]->getBoolValue() )
				{
					sprintf(buf, "3 \"%s\" 1.0 \"unitless\"\n", dimensionsBool[i]->getName().c_str());
					fputs( buf, fp );
				}
				else
				{
					sprintf(buf, "3 \"%s\" -1.0 \"unitless\"\n", dimensionsBool[i]->getName().c_str());
					fputs( buf, fp );
				}
			}
		}*/
		for ( i = 0; i < dimensionsOut.size(); i++ )
		{
			if ( dimensionsOut[i]->getExists() )
			{
				if ( dimensionsOut[i]->getHaveSegments() )
				{
					sprintf(buf, "3 \"%s\" %.4f \"%s\" %d\n", dimensionsOut[i]->getName().c_str(), dimensionsOut[i]->getValue(),
						dimensionsOut[i]->getUnit().c_str(), i);
					fputs( buf, fp );
				}
				else
				{
					sprintf(buf, "4 \"%s\" %.4f \"%s\" %d\n", dimensionsOut[i]->getName().c_str(), dimensionsOut[i]->getValue(),
						dimensionsOut[i]->getUnit().c_str(), i);
					fputs( buf, fp );
				}
			}
		}
		for ( i = 0; i < massProperties.size(); i++ )
		{
			if ( massProperties[i]->getType() > -1)
			{
				sprintf(buf, "5 \"%s\" %.4f \"%s\"\n", massProperties[i]->getName().c_str(), massProperties[i]->getValue(),
					massProperties[i]->getUnit().c_str());
				fputs( buf, fp );
			}
		}
		for ( i = 0; i < allCompExists.size(); i++ )
		{
			sprintf(buf, "%s\n", allCompExists[i].c_str());
			fputs( buf, fp );
		}
		fclose(fp);
		// now create the html file that will be loaded by dome to begin the vrml session
		sprintf(buf, "%s\\%s.html", export_directory.c_str(), topCompName.c_str());
		fp = fopen(buf, "w");
		sprintf(buf, "<HTML>\n<BODY>\n<CENTER>\n<embed src=\"%s.wrl\" border=0 width=670 height=670>\n",
			topCompName.c_str());
		fputs( buf, fp );
		sprintf(buf, "<APPLET\n  ARCHIVE=\"./VrmlGUI.jar\"\n  CODE=\"Pane.class\"\n  mayscript\n");
		fputs( buf, fp );
		sprintf(buf, "  width = 235 height=670>\n<param name=\"datafile\" value=\"%s.dat\">\n", topCompName.c_str());
		fputs( buf, fp );
		sprintf(buf, "<param name=\"pic\" value=\"middle.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"pic1\" value=\"sign1.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"pic2\" value=\"sign2.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"up\" value=\"up.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"down\" value=\"down.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"ass\" value=\"ass.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"part\" value=\"part.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"tin\" value=\"tin.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "<param name=\"texp\" value=\"texp.gif\">\n");
		fputs( buf, fp );
		sprintf(buf, "</APPLET>\n</CENTER>\n</BODY>\n</HTML>\n");
		fputs( buf, fp );
		fclose(fp);
	}
	catch (...)
	{
		UG_ERROR("UgComponent::printToVRML: unknown exception caught");
		clean();
	}

}

/*************************************************************************************
 *
 *						UgDimension						
 *
 *************************************************************************************/

UgDimension::UgDimension(string dimName)
{
	name.assign(dimName.c_str());
	value = 0.0;
	exists = false;
	changed = false; // for now: disconnect but not destroy during periods of rest
	unit.assign("unitless");
}

/*************************************************************************************
 *
 *						UgDimensionIn						
 *
 *************************************************************************************/

void UgDimensionIn::findSegments(UgPart* part)
{
	double	dim_origin[3], *array;
	int	errorCode, dim_subtype, ii, jj, type, subtype, match, n_strings;
	char buf[MAX_LN_SIZE+1]; //, buf2[MAX_LN_SIZE+1];
	UF_DRF_dim_info_t *dim_info;
	tag_t	dim, member;
	UgTypedObject	*pCurObj;
	string line; 
	UF_DRF_draft_aid_text_info_t	*text_info;
	try
	{
		dim = Exp_DOMEin->getTag();
		sprintf(buf, "okie! tag = %d", dim);
		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			member = pCurObj->getTag();
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype); 
			if ( type == UF_dimension_type ) // user has to give exact name of dimension object!!!
			{
				errorCode = UF_DRF_ask_dim_info(member, &dim_subtype, dim_origin, &dim_info); 
				match = -1;
				for (ii = 0; ii<dim_info->num_text; ii++)
				{
			        for (jj = 0; jj < dim_info->text_info[ii].num_lines; jj++) // should only be 1 for modeler
					{
						line.assign(dim_info->text_info[ii].text[jj].string);
						match = line.find(name, 0); 
						sprintf(buf, "%s to %s = %d", line.c_str(), name.c_str(), match);
						UG_DEBUG(buf); 
					}
			    }
				if (match > -1)
				{
					UG_DEBUG("found match IN!!!");
					numSegments = dim_info->num_lines;
					sprintf(buf, "num dim lines: %d", numSegments);
					UG_DEBUG(buf); 
				    if (numSegments > 0)
			        {
						haveSegments = true;
					    for (ii = 0; ii < numSegments; ii++)
						{
							for (jj=0; jj<dim_info->dim_line_info->line_pnts[ii].num_segments; jj++)
							{
								array = &dim_info->dim_line_info->line_pnts[ii].segment_pnts[jj*6];
								sprintf(buf, "1 start (%f, %f, %f)", array[0], array[1], array[2]);
								UG_DEBUG(buf);
								startX.push_back(array[0]);
								startY.push_back(array[1]);
								startZ.push_back(array[2]);
								array = &dim_info->dim_line_info->line_pnts[ii].segment_pnts[jj*6+3];
								sprintf(buf, "1 end (%f, %f, %f)", array[0], array[1], array[2]);
								UG_DEBUG(buf); 
								endX.push_back(array[0]);
								endY.push_back(array[1]);
								endZ.push_back(array[2]);  
							}
						}
						// now get starting position of dimension text
						errorCode = UF_DRF_ask_draft_aid_text_info(member, &n_strings, &text_info);
						if(errorCode)
						{
							UG_ERROR("inside findSegments");
						}
						origin.push_back(text_info[0].origin[0]);
						origin.push_back(text_info[0].origin[1]);
						origin.push_back(text_info[0].origin[2]);
						height = 2.0*text_info[0].height;
						sprintf(buf, "size of X = %d", startX.size());
						UG_DEBUG(buf);
						for (ii = 0; ii < numSegments; ii++)
						{
							sprintf(buf, "segment %d: ", ii);
							UG_DEBUG(buf);
							sprintf(buf, "start (%f, %f, %f)", startX[ii], startY[ii], startZ[ii]);
							UG_DEBUG(buf);
							sprintf(buf, "end (%f, %f, %f)", endX[ii], endY[ii], endZ[ii]);
							UG_DEBUG(buf);
						}
						sprintf(buf, "text origin (%f, %f, %f) [%f]", origin[0], origin[1], origin[2], height);
						UG_DEBUG(buf);  
					}
				}
    			errorCode = UF_DRF_free_dimension(&dim_info);
				if(errorCode)
				{
					UG_ERROR("inside findSegments");
				}
				if (match > -1)
					break;
   			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgDimensionIn::findSegments: unknown exception caught");
	}
}


void UgDimensionIn::clean() 
{ 
	Exp_DOMEin = NULL;
	exists = false;
	changed = true;
	while (!startX.empty())
		startX.pop_back();
	while (!startY.empty())
		startY.pop_back();
	while (!startZ.empty())
		startZ.pop_back();
	while (!endX.empty())
		endX.pop_back();
	while (!endY.empty())
		endY.pop_back();
	while (!endZ.empty())
		endZ.pop_back();
	while (!origin.empty())
		origin.pop_back();
	numSegments = 0;
	haveSegments = false;
}

/*************************************************************************************
 *
 *						UgDimensionBool						
 *
 *************************************************************************************/

void UgDimensionBool::setValueBool(double in)
{
	if (!changed)
	{
		if ( in > 0 )
			boolValue = true;
		else
			boolValue = false;
	}
}

/*************************************************************************************
 *
 *						UgDimensionOut						
 *
 *************************************************************************************/

void UgDimensionOut::clean()
{ 
	Exp_DOMEout = NULL;
	exists = false;
	changed = false;
	while (!startX.empty())
		startX.pop_back();
	while (!startY.empty())
		startY.pop_back();
	while (!startZ.empty())
		startZ.pop_back();
	while (!endX.empty())
		endX.pop_back();
	while (!endY.empty())
		endY.pop_back();
	while (!endZ.empty())
		endZ.pop_back();
	while (!origin.empty())
		origin.pop_back();
	numSegments = 0;
	haveSegments = false;
}

void UgDimensionOut::findSegments(UgPart* part)
{
	double	dim_origin[3], *array;
	int	errorCode, dim_subtype, ii, jj, type, subtype, match, n_strings;
	char buf[MAX_LN_SIZE+1]; //, buf2[MAX_LN_SIZE+1];
	UF_DRF_dim_info_t *dim_info;
	tag_t	dim, member;
	UgTypedObject	*pCurObj;
	string line;
	UF_DRF_draft_aid_text_info_t	*text_info;

	try
	{
		dim = Exp_DOMEout->getTag();
		sprintf(buf, "okie! tag = %d", dim);
		UG_DEBUG(buf);
		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			member = pCurObj->getTag();
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
			if ( type == UF_dimension_type ) // user has to give exact name of dimension object!!!
			{
				errorCode = UF_DRF_ask_dim_info(member, &dim_subtype, dim_origin, &dim_info);
				match = -1;
				for (ii = 0; ii<dim_info->num_text; ii++)
				{
			        for (jj = 0; jj < dim_info->text_info[ii].num_lines; jj++) // should only be 1 for modeler
					{
						line.assign(dim_info->text_info[ii].text[jj].string);
						match = line.find(name, 0);
						sprintf(buf, "%s to %s = %d", line.c_str(), name.c_str(), match);
						UG_DEBUG(buf);
					}
			    }
				if (match > -1)
				{
					UG_DEBUG("found match OUT!!!");
					numSegments = dim_info->num_lines;
					sprintf(buf, "num dim lines: %d", numSegments);
					UG_DEBUG(buf);
				    if (numSegments > 0)
			        {
						haveSegments = true;
					    for (ii = 0; ii < numSegments; ii++)
						{
							for (jj=0; jj<dim_info->dim_line_info->line_pnts[ii].num_segments; jj++)
							{
								array = &dim_info->dim_line_info->line_pnts[ii].segment_pnts[jj*6];
								startX.push_back(array[0]);
								startY.push_back(array[1]);
								startZ.push_back(array[2]);
								array = &dim_info->dim_line_info->line_pnts[ii].segment_pnts[jj*6+3];
								endX.push_back(array[0]);
								endY.push_back(array[1]);
								endZ.push_back(array[2]);
							}
						}
						// now get starting position of dimension text
						errorCode = UF_DRF_ask_draft_aid_text_info(member, &n_strings, &text_info);
						origin.push_back(text_info[0].origin[0]);
						origin.push_back(text_info[0].origin[1]);
						origin.push_back(text_info[0].origin[2]);
						height = 2.0*text_info[0].height;
						sprintf(buf, "size of X = %d", startX.size());
						UG_DEBUG(buf);
						for (ii = 0; ii < numSegments; ii++)
						{
							sprintf(buf, "segment %d: ", ii);
							UG_DEBUG(buf);
							sprintf(buf, "start (%f, %f, %f)", startX[ii], startY[ii], startZ[ii]);
							UG_DEBUG(buf);
							sprintf(buf, "end (%f, %f, %f)", endX[ii], endY[ii], endZ[ii]);
							UG_DEBUG(buf);
						}
						sprintf(buf, "text origin (%f, %f, %f) [%f]", origin[0], origin[1], origin[2], height);
						UG_DEBUG(buf);
					}
				}
    			errorCode = UF_DRF_free_dimension(&dim_info);
				if (match > -1)
					break;
   			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgDimensionOut::findSegments: unknown exception caught");
	}
}

/*************************************************************************************
 *
 *						UgMassProperty						
 *
 *************************************************************************************/

UgMassProperty::UgMassProperty(string propName)
{
	type = -1; // use as indicator to figure out whether invalid mass type was specified
	value = 0.0;
	name.assign(propName.c_str());
}

void UgMassProperty::findType(vector <string> MassName)
{
	int i;

	for ( i = 0; i < MassName.size(); i++ )
	{
		if ( !name.compare(MassName[i]) )
		{
			type = i;
			break;
		}
	}
}


/*************************************************************************************
 *
 *						UgExportFile						
 *
 *************************************************************************************/

UgExportFile::UgExportFile(string expName)
{
	name.assign(expName.c_str());
	type = -1;
	valid = false;
}

void UgExportFile::findExportType()
{
	if (!name.compare("iges"))
		type = 1;
	else
	{
		if (!name.compare("step"))
			type = 2;
		else
			type = -1; // invalid export type specified
	}
	if (type > -1)
		valid = true;
	else
		valid = false;
}

/*************************************************************************************
 *
 *						UgImportFile						
 *
 *************************************************************************************/

UgImportFile::UgImportFile(string impName)
{
	name.assign(impName.c_str());
	exists = false;
	changed = false;
	type = -1;
	valid = false;
	matingDir = 1;
	parent = 0;
}

void UgImportFile::findImportType()
{
	if (!name.compare(0, 2, "i_"))
		type = 1;
	else
	{
		if (!name.compare(0, 2, "s_"))
			type = 2;
		else
			type = -1; // invalid import type specified
	}
	if (type > -1)
		valid = true;
	else
		valid = false;
}

void UgImportFile::findFileName(string dirName)
{
	fileName.assign(dirName);
	fileName.append("\\import\\");
	fileName.append(name);
	if ( type == 1 )
		fileName.append(".igs");
	else
	{
		if ( type == 2 )
			fileName.append(".stp");
	}
}

void UgImportFile::updateChild()
{
	int i;

	while (!children.empty())
		children.pop_back();
	for ( i = 0; i < childrenNew.size(); i++ )
		children.push_back(childrenNew[i]);
	while (!childrenNew.empty())
		childrenNew.pop_back();
	while (!base.empty())
		base.pop_back();
	while (!basePart.empty())
		basePart.pop_back();
}

void UgImportFile::updateBoundingBoxForAss()
{
	int	body_type, errorCode, subtype, type, first_time = 1, j;
	char	buf[MAX_LN_SIZE+1];
	tag_t	member;
	UgTypedObject	*pCurObj;
	double bounding_box[6], bounding_box2[6], bounding_box3[6], transform[4][4];

	// this function must be invoked for the top assembly ONLY (for now) every time a new VRML file
	// is to be exported; need the bounding box to envelope the DOME components completely
	UG_DEBUG("in UgImportFile::updateBoundingBoxForAss"); 
	try
	{
		for (pCurObj = compPtr->iterateFirst(); pCurObj; pCurObj = compPtr->iterateNext(pCurObj))
		{
			member = pCurObj->getTag(); UG_DEBUG("Get Name: " << pCurObj->getName().c_str());
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
			if(errorCode)
			{
				UG_ERROR("Exiting application..."); return;
			}
			// obtain bounding box limits
			if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
			{
				UG_DEBUG("Inside this if statement...");
				errorCode = UF_MODL_ask_body_type( member, &body_type );
				if ( body_type == UF_MODL_SOLID_BODY )
				{
					errorCode = UF_MODL_ask_bounding_box( member, bounding_box );
					if(errorCode)
					{
						UG_ERROR("ERROR - exiting program");
						return;
					}
					if (first_time == 1)
					{
						for (j = 0; j < 6; j++){
							bounding_box2[j] = bounding_box[j]; UG_DEBUG("For loop value: " << bounding_box2[j]);}
						first_time = 0;
						
					}
							
					else
					{
						for (j = 0; j < 3; j++) // obtain min
						{
							if (bounding_box[j] < bounding_box2[j])
								bounding_box2[j] = bounding_box[j];
							UG_DEBUG("Second time...");
						}
						for (j = 3; j < 6; j++) // obtain max
						{
							if (bounding_box[j] > bounding_box2[j])
								bounding_box2[j] = bounding_box[j];
						}
					}
				}
			}
			// take transform of occurrence into consideration
			errorCode = UF_ASSEM_ask_transform_of_occ(instanceTag, transform);
			if(errorCode)
			{
				UG_ERROR("Exiting application...");
				return;
			}
			UG_DEBUG("transform matrix");
			sprintf(buf, "%f %f %f %f", transform[0][0], transform[0][1], transform[0][2], transform[0][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[1][0], transform[1][1], transform[1][2], transform[1][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[2][0], transform[2][1], transform[2][2], transform[2][3]);
			UG_DEBUG(buf);
			sprintf(buf, "%f %f %f %f", transform[3][0], transform[3][1], transform[3][2], transform[3][3]);
			UG_DEBUG(buf);


			bounding_box3[0] = transform[0][0]*bounding_box2[0] + transform[0][1]*bounding_box2[1] +
				transform[0][2]*bounding_box2[2] + transform[0][3];

			bounding_box3[1] = transform[1][0]*bounding_box2[0] + transform[1][1]*bounding_box2[1] +
				transform[1][2]*bounding_box2[2] + transform[1][3];

			bounding_box3[2] = transform[2][0]*bounding_box2[0] + transform[2][1]*bounding_box2[1] +
				transform[2][2]*bounding_box2[2] + transform[2][3];

			bounding_box3[3] = transform[0][0]*bounding_box2[3] + transform[0][1]*bounding_box2[4] +
				transform[0][2]*bounding_box2[5] + transform[0][3];

			bounding_box3[4] = transform[1][0]*bounding_box2[3] + transform[1][1]*bounding_box2[4] +
				transform[1][2]*bounding_box2[5] + transform[1][3];

			bounding_box3[5] = transform[2][0]*bounding_box2[3] + transform[2][1]*bounding_box2[4] +
				transform[2][2]*bounding_box2[5] + transform[2][3];
			for(int i=0;i<6;i++) UG_DEBUG(bounding_box3[i] << " - bounding box");

			// set components's boundingBox
			if (!boundingBoxAssCoord.empty())
				boundingBoxAssCoord.pop_back();
			for ( i = 0; i < 6; i++ )
				boundingBoxAssCoord.push_back(bounding_box3[i]);
		
		}
	}
	catch (...)
	{
		UG_ERROR("UgImportFile::updateBoundingBoxForAss: unknown exception caught");
		clean();
	}
}

void UgImportFile::clean()
{
	type = -1;
	valid = false;
	exists = false;
	changed = false;
	compPtr = NULL;
	parent = 0;
	while (!neutral_instance.empty())
		neutral_instance.pop_back();
	while (!children.empty())
		children.pop_back();
	while (!childrenNew.empty())
		childrenNew.pop_back();
	while (!base.empty())
		base.pop_back();
	while (!basePart.empty())
		basePart.pop_back();
	while (!boundingBoxAssCoord.empty())
		boundingBoxAssCoord.pop_back();
//	matingDir = 1;
}

/*************************************************************************************
 *
 *						UgBoolean						
 *
 *************************************************************************************/

UgBoolean::UgBoolean()
{
	value = false;
}

/*************************************************************************************
 *
 *						UgVrmlFile						
 *
 *************************************************************************************/

UgVrmlFile::UgVrmlFile(string vName)
{
	name.assign(vName.c_str());
	changed = false;
	VrmlFrame = 0;
	VrmlZoom = 1.0;
}

void UgVrmlFile::findFileName(string export_directory, string model, UgPart* compPtr)
{
	fileName.assign(export_directory);
	fileName.append("\\");
	fileName.append(model);
	fileName.append(".wrl");
	exportDir.assign(export_directory);
	modelName.assign(model); 
	part = compPtr;
}

void UgVrmlFile::autoVrmlTemplateCreate(string export_directory)
{
	char buf[MAX_LN_SIZE+1];
	FILE	*fp;

	try
	{
		sprintf(buf, "%s\\vrml.htt", export_directory.c_str());
		fp = fopen(buf, "r");
		if (fp == NULL)
		{
			fp = fopen(buf, "w");
			sprintf(buf, "<@UGVRML  FILE=\"<@UGVAR FORMAT=\"$HSPEC\">.wrl\"\n");
			fprintf(fp, buf);
			sprintf(buf, "  VERSION=\"2\" LIGHTS=\"YES\" MATERIALS=\"YES\" TEXTURES=\"YES\" BACKGROUND=\"YES\">");
			fprintf(fp, buf);
			fclose(fp);
			UG_DEBUG("finished creating vrml.htt file in autoVrmlTemplateCreate");
		}
	}
	catch (...)
	{
		UG_ERROR("UgVrmlFile::autoVrmlTemplateCreate: unknown exception caught");
		clean();
	}
}

void UgVrmlFile::exportVRML(bool isAssem, int index)
{
	char fspec[MAX_LN_SIZE+1], buf[MAX_LN_SIZE+1], line[500];
	int	i, /*errorCode*/ first, order[7];
	FILE *fp, *fp2;
	string lines;

	UG_DEBUG("UgVrmlFile::exportVRML()");
	try
	{
//		UgSession::setWorkPart( part );
		sprintf(fspec, "%s\\%s_temp", exportDir.c_str(), modelName.c_str());

		// open up file and append to it
		sprintf(buf,"%s.wrl", fspec);
		
//////////////////////////////////////////////////////////////////////////////////////////////////
		// first check to see if file was outputted, if not probably too long a file name, not
		// filepath though
//////////////////////////////////////////////////////////////////////////////////////////////////
		fp = fopen(buf, "a");

		if ( isAssem )
		{
			sprintf(buf, "\n] }\n\n");
			fputs( buf, fp );
		}

		if (index == 0)
		{
			sprintf(buf, "%s\\bd2", exportDir.c_str());
			fp2 = fopen(buf, "r");
			if ( fp2 != NULL )
			{
				while (	fgets(line, 500, fp2) )
					fputs( line, fp );
				fclose( fp2 );
			}
		}
		sprintf(buf, "\n");
		fputs( buf, fp );
		sprintf(buf, "del %s\\bd", exportDir.c_str());
		system(buf);
		sprintf(buf, "del %s\\bd2", exportDir.c_str());
		system(buf);
		for ( i = 0; i < 7; i++ )
		{
			order[i] = VrmlFrame+i;
			if ( order[i] > 6.5 )
				order[i] = order[i] - 7;
		}

		for ( i = 0; i < 7; i++ )
		{
			if ( order[i] == 0 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position  %f %f %f\n", vrml_pos[0][0], vrml_pos[0][1],
					vrml_pos[0][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[0][0], vrml_ori[0][1],
					vrml_ori[0][2], vrml_ori[0][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Isometric (0)\"\n}\n");
				fputs( buf, fp );
			}
			if ( order[i] == 1 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position  %f %f %f\n", vrml_pos[1][0], vrml_pos[1][1],
					vrml_pos[1][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[1][0], vrml_ori[1][1],
					vrml_ori[1][2], vrml_ori[1][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Front (1)\"\n}\n");
				fputs( buf, fp );
			}
			if ( order[i] == 2 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position   %f %f %f\n", vrml_pos[2][0], vrml_pos[2][1],
					vrml_pos[2][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[2][0], vrml_ori[2][1],
					vrml_ori[2][2], vrml_ori[2][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Top (2)\"\n}\n" );
				fputs( buf, fp );
			}
			if ( order[i] == 3 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position   %f %f %f\n", vrml_pos[3][0], vrml_pos[3][1],
					vrml_pos[3][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[3][0], vrml_ori[3][1],
					vrml_ori[3][2], vrml_ori[3][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Left (3)\"\n}\n");
				fputs( buf, fp );
			}
			if ( order[i] == 4 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position	 %f %f %f\n", vrml_pos[4][0], vrml_pos[4][1], 
					vrml_pos[4][2] );
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[4][0], vrml_ori[4][1],
					vrml_ori[4][2], vrml_ori[4][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Bottom (4)\"\n}\n");
				fputs( buf, fp );
			}
			if ( order[i] == 5 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position   %f %f %f\n", vrml_pos[5][0], vrml_pos[5][1],
					vrml_pos[5][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[5][0], vrml_ori[5][1],
					vrml_ori[5][2], vrml_ori[5][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Back (5)\"\n}\n");
				fputs( buf, fp );
			}
			if ( order[i] == 6 )
			{
				sprintf(buf, "Viewpoint {\n" );
				fputs( buf, fp );
				sprintf(buf, "  position   %f %f %f\n", vrml_pos[6][0], vrml_pos[6][1],
					vrml_pos[6][2]);
				fputs( buf, fp );
				sprintf(buf, "  orientation %f %f %f %f\n", vrml_ori[6][0], vrml_ori[6][1],
					vrml_ori[6][2], vrml_ori[6][3]);
				fputs( buf, fp );
				sprintf(buf, "  description \"Right (6)\"\n}\n\n");
				fputs( buf, fp );
			}
		}
		sprintf(buf, "NavigationInfo {\n       type \"EXAMINE\"\n}");
		fputs( buf, fp );

		fclose(fp); 

		sprintf(buf,"%s.wrl", fspec);
		fp = fopen(buf, "r");
		sprintf(buf,"%s", fileName.c_str());
		fp2 = fopen(buf, "w");

		first = 1;
		while (	fgets(line, 500, fp) )
		{
			if (first == 1)
			{
				fputs( line, fp2 );

//				if ( !isAssem )
//				{
					sprintf(buf, "\nDEF RecordIn Script { eventIn SFInt32 record eventOut SFInt32 store\n" );
					fputs( buf, fp2 );
					sprintf(buf, "    url \"javascript:\n" );
					fputs( buf, fp2 );
					sprintf(buf, "    function initialize() { store = -1; }\n" );
					fputs( buf, fp2 );
					sprintf(buf, "    function record(value) {\n" );
					fputs( buf, fp2 );
					sprintf(buf, "        store = value; print(store); }\"\n}\n" );
					fputs( buf, fp2 );

					sprintf(buf, "\nShape { appearance Appearance {\n" );
					fputs( buf, fp2 );
					sprintf(buf, "    material DEF MatSet Material { transparency 0.0 shininess -1.0 ambientIntensity -1.0 } } }\n" );
					fputs( buf, fp2 );
//				}

				sprintf(buf, "\nTransform {\n" );
				fputs( buf, fp2 );
				sprintf(buf, "  rotation %f %f %f %f\n", vrml_rot[0], vrml_rot[1], vrml_rot[2],
					vrml_rot[3]);
				fputs( buf, fp2 );
				sprintf(buf, "  translation %f %f %f\n", vrml_trans[0], vrml_trans[1], vrml_trans[2] );
				fputs( buf, fp2 );
				sprintf(buf, "  center %f %f %f\n", vrml_center[0], vrml_center[1], vrml_center[1] );
				fputs( buf, fp2 );
				sprintf(buf, "  children [\n");
				fputs( buf, fp2 );
				first = 0;
			}
			else
				fputs( line, fp2 );
		}
		fclose(fp);
		fclose(fp2);

		sprintf(buf, "del %s\\%s_temp.wrl", exportDir.c_str(), modelName.c_str());
		system(buf);
	}
	catch (...)
	{
		UG_ERROR("UgVrmlFile::exportVRML: unknown exception caught");
		clean();
	}
}

void UgVrmlFile::getVrmlViews()
{
	char	buf[MAX_LN_SIZE+1];
	double	l1, w1, h1;
	int i;

	for ( i = 0; i < 6; i++ )
	{
		sprintf(buf, "bbox[%d] = %f", i, bbox[i]);
		UG_DEBUG(buf);
	}

	l1 = fabs(bbox[3]-bbox[0]);
	w1 = fabs(bbox[4]-bbox[1]);
	h1 = fabs(bbox[5]-bbox[2]);
	sprintf(buf, "bounding box length = %f", l1); // X
	UG_DEBUG(buf);
	sprintf(buf, "bounding box width = %f", w1); // Y
	UG_DEBUG(buf);
	sprintf(buf, "bounding box height = %f", h1); // Z
	UG_DEBUG(buf);
	vrml_center[0]=bbox[0]+(bbox[3]-bbox[0])/2.0; // actual UG X center
	vrml_center[1]=bbox[1]+(bbox[4]-bbox[1])/2.0; // actual UG Y center
	vrml_center[2]=bbox[2]+(bbox[5]-bbox[2])/2.0; // actual UG Z center
	sprintf(buf, "center of assembly at ( %f, %f, %f )", vrml_center[0], vrml_center[1],
		vrml_center[2] );
	UG_DEBUG(buf);

	vrml_rot[0]=1;
	vrml_rot[1]=0;
	vrml_rot[2]=0;
	vrml_rot[3]=-1.57;

	vrml_trans[0]=-vrml_center[0];
	vrml_trans[1]=-vrml_center[2];
	vrml_trans[2]=-vrml_center[1];

	vrml_ori[0][0]=-0.593319;
	vrml_ori[0][1]=0.766676;
	vrml_ori[0][2]=0.245317;
	vrml_ori[0][3]=0.971965;
	vrml_ori[1][0]=0;
	vrml_ori[1][1]=0;
	vrml_ori[1][2]=0;
	vrml_ori[1][3]=0;
	vrml_ori[2][0]=1;
	vrml_ori[2][1]=0;
	vrml_ori[2][2]=0;
	vrml_ori[2][3]=-1.57;
	vrml_ori[3][0]=0;
	vrml_ori[3][1]=1;
	vrml_ori[3][2]=0;
	vrml_ori[3][3]=-1.57;
	vrml_ori[4][0]=1;
	vrml_ori[4][1]=0;
	vrml_ori[4][2]=0;
	vrml_ori[4][3]=1.57;
	vrml_ori[5][0]=0;
	vrml_ori[5][1]=1;
	vrml_ori[5][2]=0;
	vrml_ori[5][3]=-3.141;
	vrml_ori[6][0]=0;
	vrml_ori[6][1]=1;
	vrml_ori[6][2]=0;
	vrml_ori[6][3]=1.57;

	double max_l_h, max_l_w, max_w_h, max_l_w_h;

	if (l1 > h1)
		max_l_h = l1;
	else
		max_l_h = h1;

	if (l1 > w1)
		max_l_w = l1;
	else
		max_l_w = w1;

	if (w1 > h1)
		max_w_h = w1;
	else
		max_w_h = h1;

	max_l_w_h = l1;
	if (w1 > max_l_w_h)
		max_l_w_h = w1;
	if (h1 > max_l_w_h)
		max_l_w_h = h1;

	vrml_pos_orig[0][0]=1.4288*max_l_w_h; // isometric view
	vrml_pos_orig[0][1]=vrml_pos_orig[0][0];
	vrml_pos_orig[0][2]=vrml_pos_orig[0][0];
	vrml_pos_orig[1][0]=0; // front view
	vrml_pos_orig[1][1]=0;
	vrml_pos_orig[1][2]=(1.5*max_l_h)+(w1/2.0);
	vrml_pos_orig[2][0]=0; // top view
	vrml_pos_orig[2][1]=(1.5*max_l_w)+(h1/2.0);
	vrml_pos_orig[2][2]=0;
	vrml_pos_orig[3][0]=-1.0*((1.5*max_w_h)+(l1/2.0)); // left view
	vrml_pos_orig[3][1]=0;
	vrml_pos_orig[3][2]=0;
	vrml_pos_orig[4][0]=0; // bottom view
	vrml_pos_orig[4][1]=-vrml_pos_orig[2][1];
	vrml_pos_orig[4][2]=0;
	vrml_pos_orig[5][0]=0; // back view
	vrml_pos_orig[5][1]=0;
	vrml_pos_orig[5][2]=-vrml_pos_orig[1][2]; 
	vrml_pos_orig[6][0]=-vrml_pos_orig[3][0]; // right view
	vrml_pos_orig[6][1]=0;
	vrml_pos_orig[6][2]=0;
}

void UgVrmlFile::setVrmlZoomFactor( double zFactor)
{
	char buf[256];
	int	i, j;

	UG_DEBUG("in UgVrmlFile::setVrmlZoomFactor");

	if ( zFactor < 0.5 )
		VrmlZoom = 0.5;
	else
		VrmlZoom = zFactor;

	for ( i = 0; i < 7; i++ )
	{
		for ( j = 0; j < 3; j++ )
			vrml_pos[i][j] = vrml_pos_orig[i][j]*VrmlZoom;
	}

	sprintf(buf, "zoom = %f", VrmlZoom);
	UG_DEBUG(buf);
//	exportVRML(); // makes use of recordVrmlFrame function
//	UG_DEBUG("got out of exportVRML");
}

} //namespace UgPlugin
} //namespace DOME