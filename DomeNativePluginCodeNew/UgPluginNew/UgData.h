#ifndef DOME_UGDATA_H
#define DOME_UGDATA_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UgIncludes.h"

namespace DOME {
namespace UgPlugin {

class UgMassProperty 
{
  public:
    UgMassProperty(string propName);

	// functions
	void findType(vector <string> MassName);
	void clean() { type = -1; }

	// get protected values
	int getType() { return type; }
	string getName() { return name; }
	double getValue() { return value; }
	string getUnit() { return unit; }

	// for dome
	string getName2() { return string(name.c_str()); }
	string getUnit2() { return string(unit.c_str()); }

	// set protected values
	void setType(int in) { type = in; }
	void setName(string in) { name.assign(in); }
	void setValue(double in) { value = in; }
	void setUnit(string in) { unit.assign(in); }

  protected:
	int type;
	string name;
	double value;
	string unit;
};

class UgExportFile {
  public:
	UgExportFile(string expName);

	// functions
	void findExportType();
	void clean() { type = -1; valid = false; }

	// get protected values
	int getType() { return type; }
	string getName() { return name; }
	string getFileName() { return fileName; }
	bool getValid() { return valid; }

	// for dome
	string getName2() { return string(name.c_str()); }
	string getFileName2() { return string(fileName.c_str()); }

	// set protected values
	void setType(int in) { type = in; }
	void setName(string in) { name.assign(in); }
	void setFileName(string in) { fileName.assign(in); }
	void setValid(bool in) { valid = in; }

  protected:
	int type;
	string name;
	string fileName;
	bool valid;
};

class UgDimension 
{
public:
    UgDimension(string dimName);

	// get protected values
	string getName() { return name; }
	double getValue() { return value; }
	string getUnit() { return unit; }
	bool getExists() { return exists; }
	bool getChanged() { return changed; }

	// for dome
	string getName2() { return string(name.c_str()); }
	string getUnit2() { return string(unit.c_str()); }
	
	// set protected values
	void setName(string in) { name.assign(in); }
	void setValueInit(double in) { if (!changed) value = in; }
	void setValue(double in) { value = in; changed = true; }
	void setUnit(string in) { unit.assign(in); }
	void setExists(bool in) { exists = in; }
	void setChanged(bool in) { changed = in; }

  protected:
	string name;
	double value;
	string unit;
	bool exists;
	bool changed;
};

class UgDimensionIn : public UgDimension 
{
public:
	UgDimensionIn(string dimName) : UgDimension(dimName) {};

	// functions
/*	int findDimUnit();
	void setActualValue(double lF);*/
	void nullPtr() { Exp_DOMEin = NULL; }
	void clean();
	void updateValue() { Exp_DOMEin->setRHS(value); changed = false; }
	void findSegments(UgPart* part);

	// get protected values
	UgExpression * getExpDOMEin() { return Exp_DOMEin; }
	int getNumSegments() { return numSegments; }
	bool getHaveSegments() { return haveSegments; }
	double getStartX(int in) { return startX[in]; }
	double getStartY(int in) { return startY[in]; }
	double getStartZ(int in) { return startZ[in]; }
	double getEndX(int in) { return endX[in]; }
	double getEndY(int in) { return endY[in]; }
	double getEndZ(int in) { return endZ[in]; }
	double getOriginX() { return origin[0]; }
	double getOriginY() { return origin[1]; }
	double getOriginZ() { return origin[2]; }
	double getHeight() { return height; }

	// set protected values
	void setExpDOMEin(UgExpression * in) { Exp_DOMEin = in; }
	void setNumSegments(int in) { numSegments = in; }
	void setHaveSegments(bool in) { haveSegments = in; }

  protected:
	UgExpression * Exp_DOMEin;
	int numSegments;
	vector <double> startX;
	vector <double> startY;
	vector <double> startZ;
	vector <double> endX;
	vector <double> endY;
	vector <double> endZ;
	vector <double> origin;
	double height;
	bool haveSegments;
};

class UgDimensionBool : public UgDimension {
  public:
	UgDimensionBool(string dimName) : UgDimension(dimName) {};

	// functions
/*	int findDimUnit();
	void setActualValue(double lF);*/
	void nullPtr() { Exp_DOMEin = NULL; }
	void clean() { Exp_DOMEin = NULL; exists = false; changed = true; }
	void updateValue() { Exp_DOMEin->setRHS(value); changed = false; }

	// get protected values
	UgExpression * getExpDOMEin() { return Exp_DOMEin; }
	bool getBoolValue() { return boolValue; }

	// set protected values
	void setExpDOMEin(UgExpression * in) { Exp_DOMEin = in; }
	void setBoolValue(bool in) { if (in) value = 1; else value = -1; changed = true; }
	void setValueBool(double in);

  protected:
	UgExpression * Exp_DOMEin;
	bool boolValue;
};

class UgDimensionOut : public UgDimension {
  public:
	UgDimensionOut(string dimName) : UgDimension(dimName) {};

	// functions
	void nullPtr() { Exp_DOMEout = NULL; }
	void clean();
	void updateValue() { value = Exp_DOMEout->askValue(); }
	void findSegments(UgPart* part);

	// get protected values
	UgExpression * getExpDOMEout() { return Exp_DOMEout; }
	int getNumSegments() { return numSegments; }
	void setNumSegments(int in) { numSegments = in; }
	bool getHaveSegments() { return haveSegments; }
	double getStartX(int in) { return startX[in]; }
	double getStartY(int in) { return startY[in]; }
	double getStartZ(int in) { return startZ[in]; }
	double getEndX(int in) { return endX[in]; }
	double getEndY(int in) { return endY[in]; }
	double getEndZ(int in) { return endZ[in]; }
	double getOriginX() { return origin[0]; }
	double getOriginY() { return origin[1]; }
	double getOriginZ() { return origin[2]; }
	double getHeight() { return height; }

	// set protected values
	void setExpDOMEout(UgExpression * in) { Exp_DOMEout = in; }
	void setHaveSegments(bool in) { haveSegments = in; }

  protected:
	UgExpression * Exp_DOMEout;
	int numSegments;
	vector <double> startX;
	vector <double> startY;
	vector <double> startZ;
	vector <double> endX;
	vector <double> endY;
	vector <double> endZ;
	vector <double> origin;
	double height;
	bool haveSegments;
};

class UgImportFile {
  public:
    UgImportFile(string impName);

	// functions
	void findImportType();
	void findFileName(string dirName);
	void addChild(string ch) { children.push_back(ch); }
	void updateChild();
	void clean();
	void updateBoundingBoxForAss();

	// get protected values
	int getType() { return type; }
	string getName() { return name; }
	string getFileName() { return fileName; }
	bool getValid() { return valid; }
	bool getExists() { return exists; }
	bool getChanged() { return changed; }
	tag_t getNeutralInstance(int i) { return neutral_instance[i]; }
	UgPart* getCompPtr() { return compPtr; }
	int getNeutralInstanceSize() { return neutral_instance.size(); }
	int getMatingDir() { return matingDir; }
	int getColor() { return color; }
	string getChild(int i) { return children[i]; }
	int getChildrenSize() { return children.size(); }
	string getBase(int i) { return base[i]; }
	UgPart* getBasePart(int i) { return basePart[i]; }
	int getParent() { return parent; }
	bool getIsAssembly() { return isAssembly; }
	int getBoundingBoxAssCoord(int i) {return (int)boundingBoxAssCoord[i]; }
	tag_t getInstanceTag() { return instanceTag; }

	// set protected values
	void setType(int in) { type = in; }
	void setName(string in) { name.assign(in); }
	void setFileName(string in) { fileName.assign(in); }
	void setValid(bool in) { valid = in; }
	void setExists(bool in) { exists = in; }
	void setChanged(bool in) { changed = in; }
	void setNeutralInstance(tag_t in) { neutral_instance.push_back(in); }
	void setCompPtr(UgPart* in) { compPtr = in; }
	void setMatingDir(int in) { matingDir = in; }
	void setColor(int in) { color = in; }
	void setChild(int i, string in) { children[i].assign(in); }
	void setChildrenNew(string in) { childrenNew.push_back(in); }
	void setBase(string in) { base.push_back(in); }
	void setBasePart(UgPart* in) { basePart.push_back(in); }
	void setParent(int in) { parent = in; }
	void setIsAssembly(bool in) { isAssembly = in; }
	void setBoundingBoxAssCoord(int in) { boundingBoxAssCoord.push_back(in); }
	void setInstanceTag(tag_t in) { instanceTag = in; }

  protected:
	int type;
	string name;
	string fileName;
	bool valid;
	bool exists;
	bool changed;
	UgPart* compPtr;
	vector <tag_t> neutral_instance;
	int matingDir; // 1 for add suffix _zkr to next import, 2 for no
	int color;
	vector <string> children;
	vector <string> childrenNew;

	vector <string> base;
	vector <UgPart*> basePart;
	int parent;
	bool isAssembly;
	vector <double> boundingBoxAssCoord;
	tag_t instanceTag;
};

class UgVrmlFile {
  public:
	UgVrmlFile(string vName);

	// functions
	void findFileName(string export_directory, string model, UgPart* compPtr);
	void exportVRML(bool isAssem, int index);
	void getVrmlViews();
	void recordVrmlFrame(double);
	void setVrmlZoomFactor(double);
	void autoVrmlTemplateCreate(string export_directory);
	void clean() { part = NULL; changed = false; }

	// get protected values
	string getName() { return name; }
	string getFileName() { return fileName; }
	bool getChanged() { return changed; }
	double getBBox(int i) { return bbox[i]; }

	// for dome
	string getName2() { return string(name.c_str()); }
	string getFileName2() { return string(fileName.c_str()); }

	// set protected values
	void setName(string in) { name.assign(in); }
	void setFileName(string in) { fileName.assign(in); }
	void setChanged(bool in) { changed = in; }
	void setBBox(int i, double in) { bbox[i] = in; }

  protected:
	string name;
	string fileName;
	bool changed;
	// used for VRML
	double bbox[6]; // 0 for min x, 1 for min y, 2 for min z, 3 for max x, 4 for max y, 5 for max z
	double vrml_trans[3], vrml_rot[4], vrml_pos[7][3], vrml_pos_orig[7][3], vrml_ori[7][4], vrml_center[3];
	int VrmlFrame;
	double VrmlZoom;
	UgPart* part;
	string exportDir;
	string modelName;
};

class UgBoolean 
{
public:
    UgBoolean();

	// get protected values
	bool getValue() { return value; }

	// set protected values
	void setValue(bool in) { value = in; }

protected:
	bool value;
};

class UgComponent 
{ 
// provides common element to UgAssembly and UgPart
  public:
    UgComponent(string part, string analysis);
    UgComponent(string part);
    virtual UgMassProperty* createMassProperty(string propertyName);
    virtual UgExportFile* createExportFile(string expName); // only for topLevel component
    virtual UgDimensionIn* createDimensionIn(string dimName);
    virtual UgDimensionOut* createDimensionOut(string dimName);
    virtual UgDimensionBool* createDimensionBool(string dimName);

	// functions
	void findDimObjects();
	bool findDimChanged(int i) { return dimensionsIn[i]->getChanged(); }
	void prepPart();
	string findDOMEoutUnit(int type);
	void findMassValues(vector <string> MassName);
	void findAllMassValues();
	void findExportInfo(string dirName);
	void exportAll(string dirName, string export_directory, string UGII_BASE_DIR);
	void clean();
	void cleanAll();
	void updateDimInValues();
	void updateOutputValues();
	void printToVRML(string wrl, string export_directory, vector <string> allCompExists);
	void updateBoundingBoxForAss();

	// get protected values
	string getCompName() { return compName; }
	bool getTopLevel() { return topLevel; }
	bool getExists() { return exists; }
	UgExportFile* getExportFile(int i) { return exports[i]; }
	int	getExportsSize () { return exports.size(); }
	UgPart* getCompPtr() { return compPtr; }
	int getAnalysisUnit() { return analysis_unit; }
	string getDimUnit() { return dimUnit; }
	bool getIsAssembly() { return isAssembly; }
	string getTopCompName() { return topCompName; }
	double getBoundingBox(int i) { return boundingBox[i]; }
	int getCompIndex() { return compIndex; }
	double getBoundingBoxAssCoord(int i) { return boundingBoxAssCoord[i];}
	tag_t getInstanceTag() { return instanceTag; }
	int getParent() { return parent; }

	// set protected values
	void setCompName(string in) { compName.assign(in); }
	string getCompName2() { return compName.c_str(); }
	void setTopLevel(bool in) { topLevel = in; }
	void setExists(bool in) { exists = in; }
	void setCompPtr(UgPart* in) { compPtr = in; }
	void setAnalysisUnit(int in) { analysis_unit = in; }
	void setDimUnit(string in) { dimUnit.assign(in); }
	void setIsAssembly(bool in) { isAssembly = in; }
	void setTopCompName(string in) { topCompName.assign(in); }
	void setBoundingBox(double in) { boundingBox.push_back(in); }
	void setCompIndex(int in) { compIndex = in; }
	void setBoundingBoxAssCoord(double in) { boundingBoxAssCoord.push_back(in); }
	void setInstanceTag(tag_t in) { instanceTag = in; }
	void setParent(int in) { parent = in; }

  protected:
	string compName;
    bool topLevel;
	bool exists;
	int analysis_unit;
	UgPart* compPtr;  // change components[] to compPtr
    vector <UgDimensionIn*> dimensionsIn;
    vector <UgDimensionOut*> dimensionsOut;
	vector <UgDimensionBool*> dimensionsBool;
    vector <UgMassProperty*> massProperties;
    vector <UgExportFile*> exports;
	string dimUnit; // right now, only length; later add clause to find type -> consider angle
	int haveChangedDimIn;
	bool isAssembly;
	string topCompName;
	vector <double> boundingBox, boundingBoxAssCoord;
	int compIndex;
	tag_t instanceTag;
	int parent;
};




} // namespace UgPlugin
} // namespace DOME

#endif