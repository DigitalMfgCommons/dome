//----------------------------------------------------------------------------
// UgDelegate.h
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********          (EXCEPT WHERE NOTED) AS NEEDED           ************
//----------------------------------------------------------------------------


#ifndef DOME_UgDelegate_H
#define DOME_UgDelegate_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "UgModel.h"
using namespace DOME::UgPlugin;


class UgDelegate
{
public:
	UgDelegate () {}


	//////////////////////////////////////////////////////////////////////////////////////
	// entry points to this class
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (begin) ************

	void     callVoidFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								long iObjectPtr, unsigned int iFunctionIndex);

	int      callIntegerFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	double	 callDoubleFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	bool     callBooleanFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	const
	char*    callStringFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	long     callObjectFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector<int>
		callIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	vector<double>
		callDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<int> >
		call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									 long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<double> > 
		call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									long iObjectPtr, unsigned int iFunctionIndex);

	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (end) ************
	//////////////////////////////////////////////////////////////////////////////////////





	//////////////////////////////////////////////////////////////////////////////////////
	// object functions
	//////////////////////////////////////////////////////////////////////////////////////

	UgModel  *initializeModel (JNIEnv* env, jobjectArray args, unsigned int iNumArgs);

	UgComponent *createComponent (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

	UgVrmlFile *createVrmlFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

	UgImportFile *createImportFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);


	//**The following will be created by UgComponent instead of UgModel
    UgMassProperty *createMassProperty(JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

    UgExportFile *createExportFile(JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

    UgDimensionIn *createDimensionIn(JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

    UgDimensionOut *createDimensionOut(JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs, long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// void functions
	//////////////////////////////////////////////////////////////////////////////////////

	void	executeModel (long iObjectPtr);
	void	loadModel (long iObjectPtr);
	void	unloadModel (long iObjectPtr);
	void	destroyModel (long iObjectPtr);
	void	import (long iObjectPtr);
	void	export (long iObjectPtr);
	void	importfileSetChanged (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs, long iObjectPtr);
	void	setDimensionInValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs, long iObjectPtr);


	//////////////////////////////////////////////////////////////////////////////////////
	// real functions
	//////////////////////////////////////////////////////////////////////////////////////

	double getDimensionInValue (long iObjectPtr);
	double getMasspropertyValue (long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// integer functions
	//////////////////////////////////////////////////////////////////////////////////////

	int		getIntegerValue (long iObjectPtr, unsigned int iFunctionIndex);


	//////////////////////////////////////////////////////////////////////////////////////
	// boolean functions
	//////////////////////////////////////////////////////////////////////////////////////

	bool	getBooleanValue (long iObjectPtr);
	bool	isModelLoaded (long iObjectPtr);
	bool	importfileGetChanged (long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// string functions
	//////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////////////////////////////////////
	// 1D array functions
	//////////////////////////////////////////////////////////////////////////////////////



};


#endif // DOME_UgDelegate_H
