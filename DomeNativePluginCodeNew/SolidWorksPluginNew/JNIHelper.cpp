//--------------------------------------------------------------------------
// JNIHelper.cpp
//
// Purpose: retrieve an argument from a list of Java-wrapped parameters
//
//
// *********** NO MODIFICATION NECESSARY BY THE PLUGIN AUTHOR ************
//--------------------------------------------------------------------------

#include <stdafx.h>

#ifdef _WINDOWS
#include <windows.h>	// WideCharToMultiByte
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include <iostream>
#include "JNIHelper.h"

using namespace std;


char g_szJavaDataTypes[][50] = {
	"java/lang/String",
	"java/lang/Boolean",
	"java/lang/Double",
	"java/lang/Integer",
	"[I",					// 1D array of integers
	"[D",					// 1D array of doubles
	"[[I",					// 2D array of integers
	"[[D"					// 2D array of doubles
};



//Unicode to local charset conversion
char* jstringToLocalChars( JNIEnv* env, jstring jstr )
{
	int length = env->GetStringLength( jstr );
	const jchar* jcstr = env->GetStringChars( jstr, 0 );
	char* rtn = new char[length*2+1];
	int size = 0;

	#ifdef _WINDOWS
		size = WideCharToMultiByte( CP_ACP, 0, (LPCWSTR)jcstr, length, rtn,
									(length*2), NULL, NULL );
	#else
		strcpy (rtn, (const char*) jcstr);
		size = strlen(rtn);
	#endif

	if( size <= 0 )
		return NULL;
	env->ReleaseStringChars( jstr, jcstr );
	rtn[size] = '\0';
	return rtn;
}


// extract a C++ object from an array of Java objects
int getArgument (JNIEnv* env, jobjectArray args, int iArgIndex, int iType, void *&value)
{
	if (env == NULL || args == NULL)
	{
		return ERR_INVALID_JAVA_OBJECT;
	}
	if (iArgIndex >= env->GetArrayLength(args))
	{
		return ERR_INVALID_ARGUMENT_NUMBER;
	}
	else
	if (iType >= NUM_JAVA_DATA_TYPES)
	{
		return ERR_INVALID_TYPE;
	}


	try
	{

		jclass cls = env->FindClass(g_szJavaDataTypes[iType]);
		jobject obj = env->GetObjectArrayElement(args, iArgIndex);
		
		//for debug
		//cout<<"Get into getArgument("<<iArgIndex<<")! The type is Types["<<iType<<"]: "<<g_szJavaDataTypes[iType]<<endl;

		if (env->IsInstanceOf(obj, cls) == JNI_TRUE)
		{
			//for debug
			//cout<<"Type matches!"<<endl;

			if (iType == JAVA_STRING)
			{
				char* str = jstringToLocalChars( env, (jstring)obj );
				value = str;
			}
			else 
			if (iType == JAVA_BOOLEAN)
			{
				jmethodID mid = env->GetMethodID(cls, "booleanValue", "()Z");
				if(mid == 0) {
					cout << "java.lang.Boolean booleanValue method not found" << endl;
					return ERR_INVALID_JAVA_CALL;
				}
				jboolean val = env->CallBooleanMethod(obj, mid);
				bool* pbValue = new bool[1];
				pbValue[0] = val;
				value = pbValue;
			}
			else 
			if (iType == JAVA_DOUBLE)
			{
				jmethodID mid = env->GetMethodID(cls, "doubleValue", "()D");
				if(mid == 0) {
					cout << "java.lang.Double doubleValue method not found" << endl;
					return ERR_INVALID_JAVA_CALL;
				}
				jdouble val = env->CallDoubleMethod(obj, mid);
				double* pdValue = new double[1];
				pdValue[0] = val;
				value = pdValue;
			}
			else 
			if (iType == JAVA_INTEGER)
			{
				jmethodID mid = env->GetMethodID(cls, "intValue", "()I");
				if(mid == 0) {
					cout << "java.lang.Integer intValue method not found" << endl;
					return ERR_INVALID_JAVA_CALL;
				}
				jint val = env->CallIntMethod(obj, mid);
				int* piValue = new int[1];
				piValue[0] = val;
				value = piValue;
			}
			else 
			if (iType == JAVA_1D_DOUBLE_ARRAY)
			{
				int rows = env->GetArrayLength((jarray)obj);
				double* doublearray = new double[rows];
				jdouble* dub = env->GetDoubleArrayElements((jdoubleArray)obj, 0);
				for(int i = 0; i < rows; i++) {
					doublearray[i] = dub[i];
				}
				env->ReleaseDoubleArrayElements((jdoubleArray)obj, dub, 0);
				value = doublearray;
			}
			else 
			if (iType == JAVA_1D_INTEGER_ARRAY)
			{
				int rows = env->GetArrayLength((jarray)obj);
				int* intarray = new int[rows];
				jint* dub = env->GetIntArrayElements((jintArray)obj, 0);
				for(int i = 0; i < rows; i++) {
					intarray[i] = (int)dub[i];									
				}
				env->ReleaseIntArrayElements((jintArray)obj, dub, 0);
				value = intarray;
			}
			else
			if (iType == JAVA_2D_DOUBLE_ARRAY)
			{
				//for debug
				//cout<<"It's JAVA_2D_DOUBLE_ARRAY!"<<endl;

/*				int cols, rows;
				double* doubleArray = NULL;

				rows = env->GetArrayLength((jarray)obj);
				if (rows > 0) {
					jdoubleArray darr = (jdoubleArray)env->GetObjectArrayElement((jobjectArray)obj, 0);
					cols = env->GetArrayLength(darr);
					doubleArray = new double[rows*cols];
				}

				for(int i = 0; i < rows; i++) {
					jdoubleArray darr = (jdoubleArray)env->GetObjectArrayElement((jobjectArray)obj, i);
					jdouble* dub = env->GetDoubleArrayElements(darr, 0);
					if(dub == 0) {
						return ERR_JNI_INTERNAL;
					}
					for(int j = 0; j < cols; j++) {
						doubleArray[i*cols + j] = dub[j];
					}
					env->ReleaseDoubleArrayElements(darr, dub, 0);
				}
				value = doubleArray;
*/
				int rows = env->GetArrayLength((jarray)obj);
				int cols = 0;
				double** doubleArray = new double*[rows];

				for(int i = 0; i < rows; i++) {
					jdoubleArray darr = (jdoubleArray)env->GetObjectArrayElement((jobjectArray)obj, i);
					jdouble* dub = env->GetDoubleArrayElements(darr, 0);
					if(dub == 0) {
						return ERR_JNI_INTERNAL;
					}
					cols = env->GetArrayLength(darr);
					doubleArray[i] = new double[cols];
					for(int j = 0; j < cols; j++) {
						doubleArray[i][j] = dub[j];	
						//for debug
						//cout<<"("<<i<<","<<j<<"): "<<dub[j]<<endl;
					}
					env->ReleaseDoubleArrayElements(darr, dub, 0);
				}
				value = doubleArray;

			}
			else 
			if (iType == JAVA_2D_INTEGER_ARRAY)
			{
				int cols, rows;
				int* intArray = NULL;

				rows = env->GetArrayLength((jarray)obj);
				if (rows > 0) {
					jintArray darr = (jintArray)env->GetObjectArrayElement((jobjectArray)obj, 0);
					cols = env->GetArrayLength(darr);
					intArray = new int[rows*cols];
				}

				for(int i = 0; i < rows; i++) {
					jintArray iarr = (jintArray)env->GetObjectArrayElement((jobjectArray)obj, i);
					jint* dub = env->GetIntArrayElements(iarr, 0);
					if(dub == 0) {
						return ERR_JNI_INTERNAL;
					}
					for(int j = 0; j < cols; j++) {
						intArray[i*cols + j] = (int)dub[j];									
					}
					env->ReleaseIntArrayElements(iarr, dub, 0);
				}
				value = intArray;
			}
		} // if
	}
	catch (...)
	{
		return ERR_JNI_INTERNAL;
	}


	return ERR_OK;
}



