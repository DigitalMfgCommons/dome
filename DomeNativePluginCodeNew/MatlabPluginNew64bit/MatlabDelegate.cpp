//----------------------------------------------------------------------------
// MatlabDelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <vector>
#include <string>
#include <iostream>
using namespace std;

// java native interface stuff
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include "JNIHelper.h"

// Matlab function prototypes
#include "MatlabIncludes.h"

// data and model classes
#include "MatlabData.h"
#include "MatlabModel.h"
using namespace DOME::MatlabPlugin;

// definitions for this class
#include "MatlabPluginCaller.h"
#include "MatlabDelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long MatlabDelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case MatlabPluginCaller_MODEL_CREATE_REAL:
		iObjectPtr = (long) createModelObject (env, args, iNumArgs, iObjectPtr);
		break;

	case MatlabPluginCaller_MODEL_CREATE_MATRIX:
		iObjectPtr = (long) createMatrix (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATLAB_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void MatlabDelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_MODEL_LOAD:
		loadModel (iObjectPtr);
		break;

	case MatlabPluginCaller_MODEL_EXECUTE:
		executeModel (iObjectPtr);
		break;

	case MatlabPluginCaller_MODEL_EXEC_BF_INPUT:
		break;

	case MatlabPluginCaller_MODEL_EXEC_AF_INPUT:
		break;

	case MatlabPluginCaller_MODEL_UNLOAD:
		unloadModel (iObjectPtr);
		break;

	case MatlabPluginCaller_MODEL_DESTROY:
		destroyModel (iObjectPtr);
		break;

	case MatlabPluginCaller_REAL_SET_VALUE:
		setRealValue (env, args, iNumArgs, iObjectPtr);
		break;

	case MatlabPluginCaller_MATRIX_SET_ELEMENT:
		setMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	case MatlabPluginCaller_MATRIX_SET_VALUES:
		setMatrixValues (env, args, iNumArgs, iObjectPtr);
		break;

	case MatlabPluginCaller_MATRIX_SET_ROWS:
	case MatlabPluginCaller_MATRIX_SET_COLS:
		setMatrixDimension (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case MatlabPluginCaller_MATRIX_SET_DIM:
		setMatrixDimensions (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATLAB_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int MatlabDelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_MATRIX_GET_ROWS:
	case MatlabPluginCaller_MATRIX_GET_COLS:
		value = getIntegerValue (iObjectPtr, iFunctionIndex);
		break;

	default:
		MATLAB_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool MatlabDelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (iObjectPtr);
		break;
		
	default:
		MATLAB_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double MatlabDelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into MatlabDelegate::callDoubleFunctions!" << endl;
	//cout << "iFunctionIndex = " << iFunctionIndex << ", MatlabPluginCaller_REAL_GET_VALUE = "<< MatlabPluginCaller_REAL_GET_VALUE << endl;
	
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_REAL_GET_VALUE:
		value = getDoubleValue (iObjectPtr);
		break;

	case MatlabPluginCaller_MATRIX_GET_ELEMENT:
		value = getMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATLAB_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const
char* MatlabDelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char *value = NULL;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATLAB_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
MatlabDelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATLAB_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
MatlabDelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATLAB_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
MatlabDelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATLAB_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
MatlabDelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MatlabPluginCaller_MATRIX_GET_VALUES:
		result = getMatrixElements (iObjectPtr);
		break;

	default:
		MATLAB_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void MatlabDelegate::loadModel (long iObjectPtr)
{
	MatlabModel *model = (MatlabModel*) iObjectPtr;

	if (model == NULL) {
		MATLAB_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void MatlabDelegate::unloadModel (long iObjectPtr)
{
	MatlabModel *model = (MatlabModel*) iObjectPtr;

	if (model == NULL) {
		MATLAB_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->unloadModel ();
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void MatlabDelegate::destroyModel (long iObjectPtr)
{
	MatlabModel *model = (MatlabModel*) iObjectPtr;

	if (model == NULL) {
		MATLAB_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}


/////////////////////////////////////////////////////
// set values
/////////////////////////////////////////////////////
void MatlabDelegate::setRealValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
  cout << "HERE\n";
	// check argument list length
	if (iNumArgs != 1) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setValue function");
	}
	else 
	{
		//MatlabReal *parameter = (MatlabReal*) iObjectPtr;
		MatlabData *parameter = (MatlabData*) iObjectPtr;
		if (parameter == NULL) {
			MATLAB_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_DOUBLE, value);
			if (err != ERR_OK) {
				MATLAB_DEBUG ("setValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				cout << "setRealValue: " << *((double*)value) << endl;
				((MatlabReal*)parameter)->setValue ( *((double*)value) );
			}

			// clean up
			if (value != NULL) delete (double*) value;
		}
	}
}


/////////////////////////////////////////////////////
// set matrix element
/////////////////////////////////////////////////////
void MatlabDelegate::setMatrixElement (JNIEnv* env, jobjectArray args,
									   unsigned int iNumArgs,
									   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 3) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixElement function");
	}
	else 
	{
		int err1, err2, err3;
		void *row = NULL;
		void *col = NULL;
		void *value = NULL;


		// unpack the argument and call the function
		//MatlabMatrix *parameter = (MatlabMatrix*) iObjectPtr;
		MatlabData *parameter = (MatlabData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);
		err3 = getArgument (env, args, 2, JAVA_DOUBLE, value);

		if (parameter == NULL) {
			MATLAB_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid row "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid column "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid parameter value "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			((MatlabMatrix*)parameter)->setElement (*((int*)row), 
								   *((int*)col),
								   *((double*)value) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
		if (value != NULL) delete (double*) value;
	}
}


/////////////////////////////////////////////////////
// set matrix values
/////////////////////////////////////////////////////
void MatlabDelegate::setMatrixValues (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs,
									  long iObjectPtr)
{

	//for debug
	//cout<<"Get into MatlabDelegate::setMatrixValues!" << endl;
	
	// check argument list length
	if (iNumArgs != 3) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixValues function");
	}
	else 
	{
		int err1, err2, err3;
		void *rows = NULL;
		void *cols = NULL;
		void *values = NULL;


		// unpack the argument and call the function
		//MatlabMatrix *parameter = (MatlabMatrix*) iObjectPtr;
		//*****iObjectPtr should be cast to MatlabData first since it has been sent to dome as MatlabData
		//*****otherwise, there will be a problem
		MatlabData *parameter = (MatlabData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_2D_DOUBLE_ARRAY, values);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, cols);

		//for debug
		cout<<"MatlabDelegate::setMatrixValues unpack parameters!" << endl;

		if (parameter == NULL) {
			MATLAB_DEBUG ("setMatrixValues: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixValues: Invalid parameter value "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixValues: Invalid number of rows "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixValues: Invalid number of columns "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			// create matrix
			int iCols = *((int*)cols);
			int iRows = *((int*)rows);

			//for debug
			//cout<< "Rows: " << iRows << ", Cols:" << iCols << endl;

			double** dValues = (double**)values;

			//for debug
			//cout<< "Pointer of values: " << values  << endl;

			//*****Two ways to create: vector< vector<double> > mat
			//Way one:
/*			vector< vector<double> > mat;
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> row;
				for (int j = 0; j < iCols; j++) {
					//for debug
					cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;

					row.push_back(dValues[i][j]);
				}
				mat.push_back(row);
			}
*/
			//Way two:
			vector <vector <double> > mat(iRows);
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> vec(iCols);
				for (int j = 0; j < iCols; j++) {
					vec[j] = dValues[i][j];
					
					//for debug
					//cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;
				}	
				mat[i] = vec;
			}

			//for debug
			cout<<"MatlabDelegate::setMatrixValues is done!" << endl;

			// set matrix values
			((MatlabMatrix*)parameter)->setValues (mat); //There is a problem to pass STL class as an argument

		}

		// clean up
		if (values != NULL) delete (double**) values;
		if (rows != NULL) delete (int*) rows;
		if (cols != NULL) delete (int*) cols;
	}
}



/////////////////////////////////////////////////////
// set number of rows or columns for a matrix
/////////////////////////////////////////////////////
void MatlabDelegate::setMatrixDimension (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	// check argument list length
	if (iNumArgs != 1) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimension function");
	}
	else 
	{
		int err;
		void *dim = NULL;

		// unpack the argument and call the function
		//MatlabMatrix *parameter = (MatlabMatrix*) iObjectPtr;
		MatlabData *parameter = (MatlabData*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_INTEGER, dim);

		if (parameter == NULL) {
			MATLAB_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid dimension "
						 << "(ERR = " << err << ")" << endl);
		}
		else {
			switch (iFunctionIndex)
			{
			case MatlabPluginCaller_MATRIX_SET_COLS:
				((MatlabMatrix*)parameter)->setColumns ( *((int*)dim) );
				break;

			case MatlabPluginCaller_MATRIX_SET_ROWS:
				((MatlabMatrix*)parameter)->setRows ( *((int*)dim) );
				break;

			default:
				MATLAB_DEBUG ("setMatrixDimension: Undefined function index ("
							  << iFunctionIndex << ")" << endl);
			}
		}

		// clean up
		if (dim != NULL) delete (int*) dim;
	}
}


/////////////////////////////////////////////////////
// set matrix dimensions
/////////////////////////////////////////////////////
void MatlabDelegate::setMatrixDimensions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 2) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimensions function");
	}
	else 
	{
		int err1, err2;
		void *rows = NULL;
		void *cols = NULL;


		// unpack the argument and call the function
		//MatlabMatrix *parameter = (MatlabMatrix*) iObjectPtr;
		MatlabData *parameter = (MatlabData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, rows);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, cols);

		if (parameter == NULL) {
			MATLAB_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid number of rows "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("setMatrixElement: Invalid number of columns "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else {
			((MatlabMatrix*)parameter)->setDimension (*((int*)rows),
									 *((int*)cols));
		}

		// clean up
		if (cols != NULL) delete (int*) cols;
		if (rows != NULL) delete (int*) rows;
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
MatlabModel *MatlabDelegate::initializeModel (JNIEnv* env, jobjectArray args,
											  unsigned int iNumArgs)
{
	MatlabModel *model = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2, err3;
		void* szFileName = NULL;
		void* bIsVisible = NULL;
		void* bIsExecutable = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szFileName);
		err2 = getArgument (env, args, 1, JAVA_BOOLEAN, bIsVisible);
		err3 = getArgument (env, args, 2, JAVA_BOOLEAN, bIsExecutable);

		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("initializeModel: Invalid model file name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("initializeModel: Invalid argument: bIsVisible (ERR = " 
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATLAB_DEBUG ("initializeModel: Invalid argument: bIsExecutable (ERR = " 
						 << err3 << ")" << endl);
		}
		else {
			// call the constructor
			model = new MatlabModel (  (char*)szFileName, 
									 *((bool*)bIsVisible),
									 *((bool*)bIsExecutable));
		}

		// clean up
		if (szFileName != NULL) delete (char*) szFileName;
		if (bIsVisible != NULL)	delete (bool*) bIsVisible;
		if (bIsExecutable != NULL)	delete (bool*) bIsExecutable;
	}

	return model;
}


///////////////////////////////////////////
// create individual model objects
///////////////////////////////////////////
MatlabData *MatlabDelegate::createModelObject (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	MatlabData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createModelObject function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		MatlabModel *model = (MatlabModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			MATLAB_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			MATLAB_DEBUG ("createModelObject: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			modelObject = model->createReal (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}


///////////////////////////////////////////
// create matrix
///////////////////////////////////////////
MatlabData *MatlabDelegate::createMatrix (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	MatlabData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createMatrix function" << endl);
	}
	else 
	{
		int err1, err2, err3;
		void* szParameterName = NULL;
		void* rows = NULL;
		void* cols = NULL;

		// unpack the arguments
		MatlabModel *model = (MatlabModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, cols);

		if (model == NULL) {
			MATLAB_DEBUG ("createMatrix: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("createMatrix: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("createMatrix: Invalid number of rows (ERR = "
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATLAB_DEBUG ("createMatrix: Invalid number of columns (ERR = "
						 << err3 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createMatrix ((char*)szParameterName,
											   *((int*)rows),
											   *((int*)cols));
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
		if (rows != NULL) delete (int*) rows;
		if (cols != NULL) delete (int*) cols;
	}

	return modelObject;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
double MatlabDelegate::getDoubleValue (long iObjectPtr)
{
	//for debug
	//cout << "Get into MatlabDelegate::getDoubleValue" << endl;

	double value = 0;
	//MatlabReal *data = (MatlabReal*) iObjectPtr;
	MatlabData *data = (MatlabData*) iObjectPtr;

	if (data == NULL) {
		MATLAB_DEBUG ("getDoubleValue: MatlabReal does not exist" << endl);
	}
	else {
		// get the value
		value = ((MatlabReal*)data)->getValue ();
	}

	return value;
}


///////////////////////////////////////////
// create matrix
///////////////////////////////////////////
double MatlabDelegate::getMatrixElement (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	double value = 0;

	// check argument list length
	if (iNumArgs != 2) {
		MATLAB_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the getMatrixElement function" << endl);
	}
	else 
	{
		int err1, err2;
		void* row = NULL;
		void* col = NULL;

		// unpack the arguments
		//MatlabMatrix *data = (MatlabMatrix*) iObjectPtr;
		MatlabData *data = (MatlabData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);

		if (data == NULL) {
			MATLAB_DEBUG ("getMatrixElement: MatlabMatrix does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATLAB_DEBUG ("getMatrixElement: Invalid row (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATLAB_DEBUG ("getMatrixElement: Invalid column (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			value = ((MatlabMatrix*)data)->getElement (*((int*)row),
									  *((int*)col) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
	}

	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get an integer value
///////////////////////////////////////////
int MatlabDelegate::getIntegerValue (long iObjectPtr,
									 unsigned int iFunctionIndex)
{
	int value = 0;

	//MatlabMatrix *data = (MatlabMatrix*) iObjectPtr;
	MatlabData *data = (MatlabData*) iObjectPtr;

	if (data == NULL) {
		MATLAB_DEBUG ("getIntegerValue: MatlabMatrix does not exist" << endl);
	}
	else {
		switch (iFunctionIndex)
		{
		case MatlabPluginCaller_MATRIX_GET_ROWS:
			value = ((MatlabMatrix*)data)->getRows ();
			break;

		case MatlabPluginCaller_MATRIX_GET_COLS:
			value = ((MatlabMatrix*)data)->getColumns ();
			break;

		default:
			MATLAB_DEBUG ("getIntegerValue: Undefined function index ("
						  << iFunctionIndex << ")" << endl);
		}
	}

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
void MatlabDelegate::executeModel (long iObjectPtr)
{
	MatlabModel *model = (MatlabModel*) iObjectPtr;

	if (model == NULL) {
		MATLAB_DEBUG ("executeModel: MatlabModel does not exist" << endl);
	}
	else {
		// execute the model
		model->execute ();
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool MatlabDelegate::getBooleanValue (long iObjectPtr)
{
	bool value = false;
	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool MatlabDelegate::isModelLoaded (long iObjectPtr)
{
	bool value = false;
	MatlabModel *model = (MatlabModel*) iObjectPtr;

	if (model == NULL) {
		MATLAB_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a string parameter
///////////////////////////////////////////
const char* MatlabDelegate::getStringValue (long iObjectPtr)
{
	const char* value = NULL;
	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 1D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////
vector<double> 
MatlabDelegate::getDoubleArrayValue (JNIEnv* env, jobjectArray args,
									 unsigned int iNumArgs, long iObjectPtr)
{
	vector<double> value;
	return value;
}




//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 2D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//////////////////////////////////////////////////////
// get the values from a 2D double array parameter
//////////////////////////////////////////////////////
vector< vector<double> > 
MatlabDelegate::getMatrixElements (long iObjectPtr)
{
	vector< vector<double> > value;

	// unpack the arguments
	//MatlabMatrix *data = (MatlabMatrix*) iObjectPtr;
	MatlabData *data = (MatlabData*) iObjectPtr;

	if (data == NULL) {
		MATLAB_DEBUG ("getMatrixElements: MatlabMatrix does not exist" << endl);
	}
	else
	{
		// call the function
		value = ((MatlabMatrix*)data)->getValues ();
	}

	return value;
}
