// MatlabModel.cpp: implementation of the MatlabModel class.
//
//////////////////////////////////////////////////////////////////////

#include "MatlabModel.h"

#ifdef _WINDOWS
#define _WIN32_WINNT 0x0400 // required for multithreaded C must be before #include objbase.h
#include <objbase.h>	// CoInitializeEx, CoUnInitializeEx
#else
#define _MAX_PATH FILENAME_MAX
#endif

#include <string>
#include <string.h>
#include <time.h>


namespace DOME {
namespace MatlabPlugin {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MatlabModel::MatlabModel(string filename, bool isVisible, bool isExecutable) throw(DomeException)
{
	m_filename = filename;
	m_engine = NULL;
	m_isVisible = isVisible;
	m_isExecutable = isExecutable;
	cout << "filename = " << filename << " length = " << filename.size() << endl;
	cout << "m_isVisible = " << m_isVisible << endl;
	cout << "m_isExecutable = " << m_isExecutable << endl;
}

MatlabModel::~MatlabModel()
{
	cout << "in destructor" << endl;

	unloadModel();
	cout<<"         deleting"<<endl;

	for (int i = 0; i < m_data.size(); i++) {
		delete m_data[i];
		m_data[i] = NULL;
	}
}

bool MatlabModel::isModelLoaded()
{
	return (m_engine != NULL);
}

void MatlabModel::loadModel() throw(DomeException)
{
	if (isModelLoaded()) return;	

	try {
		// initialize multithreaded COM library
		#ifdef _WINDOWS
			CoInitializeEx(NULL, COINIT_MULTITHREADED);
		#endif

		cout<<"********" << endl;
		
		int openStatus;
		//m_engine = engOpen(""); // so each model will have its own instance of MATLAB
		m_engine = engOpen(NULL); // See http://www.mathworks.com/help/matlab/apiref/engopen.html 
		if(m_engine == NULL) {
			MATLAB_ERROR("MatlabModel::loadModel: unable to start Matlab engine.");
		} else {
			#ifdef _MATLAB_R14
				// set engine invisible
				if (engSetVisible(m_engine, m_isVisible)) {
					MATLAB_ERROR("MatlabModel::loadModel: unable to make workspace visible.");
				}
			#endif
			#ifdef _MATLAB_R13
				// set engine invisible
				if (engSetVisible(m_engine, m_isVisible)) {
					MATLAB_ERROR("MatlabModel::loadModel: unable to make workspace visible.");
				}
			#endif
			#ifdef _MATLAB
				// set engine invisible
				if (engSetVisible(m_engine, m_isVisible)) {
					MATLAB_ERROR("MatlabModel::loadModel: unable to make workspace visible.");
				}
			#endif
			cout<<"engine = " << m_engine << endl;
			cout<<"********" << endl;

#ifdef _BUFFER_MATLAB_OUTPUT
			// set up an output buffer to catch EvalString output
			engOutputBuffer(m_engine, m_buffer, _OUTPUT_BUFFER_SIZE);
#endif
		}
	}
	catch (...) {
		MATLAB_ERROR ("MatlabModel::loadModel -- unable to open engine.");
	}

	try 
	{
		////////////////////////////////////////////////////////////////////////////
		// change the working directory to where the model file is located
		if (m_isExecutable)
		{
			char path[_MAX_PATH+1];
			sprintf (path, "cd %s", m_filename.c_str());
			char *pos = strrchr (path, '\\');	// find the bottom-most subdirectory
			if (pos != NULL) {
				*pos = '\0';
			}
			else {
				pos = strrchr (path, '//');		// find the bottom-most subdirectory
				if (pos != NULL) {
					*pos = '\0';
				}
				else {
					pos = strrchr (path, ':');	// find the drive letter
					if (pos == NULL) {
						path[0] = '\0';			// no path or drive letter
					}
					else {
						*(pos+1) = '\0';
						cout << "path = " << path << endl;
					}
				}
			}

			// display path statement
			if (strlen (path))
			{
				// change the path before each model execution
				m_commands.push_back(string(path));

			}
		}

/*		if (m_isExecutable)
		{
			string path ("cd " + m_filename);
			//for debug
			//cout << "File path and name: " << m_filename << endl;

			int pos = path.find_last_of ('\\');		// find the bottom-most subdirectory
			if (pos) {
				path[pos] = '\0';
			}
			else {
				pos = path.find_last_of ('/');		// find the bottom-most subdirectory
				if (pos) {
					path[pos] = '\0';
				}
				else {
					pos = path.find_last_of (':');	// find the drive letter
					if (pos == 0) {
						path[0] = '\0';				// no path or drive letter
					}
					else {
						path[pos+1] = '\0';
					}
				}
			}

			// display path statement
			if (path.length())
			{
				// change the path
				if (engEvalString(m_engine, path.c_str())) {
					cout << "Matlab could not do the following:" << endl;
					cout << "'" << path << "'" << endl;
					cout << "Matlab session not running" << endl;
				}
				else {
					cout << "Matlab engine executed:" << endl;
					cout << "'" << path << "' ..." << endl;
				}

			}
		}
*/
		////////////////////////////////////////////////////////////////////////////


		// read the model file
		ifstream matlabFileStream(m_filename.c_str());
		if(!matlabFileStream) {
			MATLAB_ERROR("MatlabModel::loadModel: unable to open Matlab file.");
		}
		while (matlabFileStream)
		{
			//Meena Defined a larger size for commandLine because engevalString need loop in one line
			//char commandLine[200];
			matlabFileStream.getline(commandLine, _OUTPUT_BUFFER_SIZE, '\n');
			m_commands.push_back(string(commandLine));
		}
		matlabFileStream.close ();
		createConnections();
	


	} catch(DomeException) {
		throw;
	} catch(...) {
		MATLAB_ERROR("MatlabModel::loadModel -- unable to read model file");
	}
}

void MatlabModel::unloadModel() throw(DomeException)
{
	try{
		if (isModelLoaded())
		{
			destroyConnections();

			if(m_engine != NULL)
			{
				// turn off output buffering
				engOutputBuffer(m_engine,NULL,0);

				if (!engClose(m_engine))
					cout << "Engine closed" << endl;
				else
					cout << "Engine did not close successfully" << endl;
				m_engine = NULL;
			}
			//m_commands.clear();

			// clean up COM library
			#ifdef _WINDOWS
				CoUninitialize ();
			#endif

			cout << "unloaded model" << endl;
		}
	} catch(...) {
		throw;
	}
}

void MatlabModel::execute() throw(DomeException)
{
	cout << "in execute" << endl;
	const char*  pFind;
	int lenCmd ;
	std::size_t  foundLoad ;

	if (m_engine == NULL) {
		MATLAB_ERROR("MatlabModel::execute: MLink pointer is null. Unable to execute model.");
	}
	for (int i=0; i<m_commands.size(); i++)
	{
		try {

			// Check if Comments - we check only if it started with %
			string sTmp = m_commands[i].c_str();
			string sTmp1 = sTmp.substr(0,1);
			string sPercent = string("%");
			string sLoad = "load";
			

			if (sTmp1.compare(sPercent) != 0) {

				//for debug
				cout <<"Executing: " << m_commands[i].c_str() << endl;
				// MG: Check if we have a load() command - Loading a large mat file takes time 
				// so pause a bit after the command
				foundLoad = sTmp.find(sLoad);

				lenCmd = strlen(m_commands[i].c_str()) ;

			


				// execute command
			
				if (engEvalString(m_engine, m_commands[i].c_str())) {
					cout << "Matlab session is not running" << endl;
				}
				else {
					cout << "Matlab evaluated this command" << endl;
					if (foundLoad != std::string::npos)  {
					   cout <<"Found Load in the command"<< endl;
	
						wait(10);
					}
					// If len of command is high pause accordingly - note all loops are compressed to one line
					wait(lenCmd/200); 
				
				}
		    } 
			

			// display EvalString output
#ifdef _BUFFER_MATLAB_OUTPUT
			cout << "engEvalString produced the following result:\n" << m_buffer << endl;
#endif
		}
		catch (...) {
			MATLAB_ERROR ("Could not execute model.");
		}
	}

	//for debug
	cout << "Finish execution!" << endl;
}

MatlabReal* MatlabModel::createReal(string name) throw(DomeException)
{
	MatlabReal* real = new MatlabReal(name);
	m_data.push_back(real);
	cout << "created '" << name << "'" << endl;
	return real;
}

MatlabMatrix* MatlabModel::createMatrix(string name, int rows, int columns) throw(DomeException)
{
	MatlabMatrix* matrix = new MatlabMatrix(name, rows, columns);
	m_data.push_back(matrix);
	cout << "created '" << name << "'" << endl;
	return matrix;
}


void MatlabModel::createConnections() throw(DomeException)
{
	try {
		for (int i=0; i<m_data.size(); i++)
		{
			m_data[i]->connect(m_engine);
		}
	} catch(...) {
		throw;
	}
}

void MatlabModel::destroyConnections() throw(DomeException)
{
	for (int i=0; i<m_data.size(); i++)
	{
		m_data[i]->disconnect ();
	}
}

void MatlabModel::displayDataNames()
{
	for (int i=0; i<m_data.size(); i++)
	{
		cout << m_data[i]->m_name << endl;
	}
}

void MatlabModel::wait( int seconds )
{
	clock_t endwait;
	endwait = clock () + seconds * CLOCKS_PER_SEC ;
	while (clock() < endwait) {
	}
}

} //namespace MatlabPlugin
} //DOME
