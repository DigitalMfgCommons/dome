// MatlabModel.h: interface for the MatlabModel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATLABMODEL_H
#define DOME_MATLABMODEL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MatlabData.h"
#include <fstream>
using std::ifstream;

namespace DOME {
namespace MatlabPlugin {

class MatlabModel : public DomeModel  
{
public:
	MatlabModel(string filename, bool isVisible, bool isExecutable) throw(DomeException);
	virtual ~MatlabModel();

	MatlabReal* createReal(string name) throw(DomeException);
	MatlabMatrix* createMatrix(string name, int rows, int columns) throw(DomeException);
	
	bool isModelLoaded();
	void loadModel() throw(DomeException);
	void unloadModel() throw(DomeException);

	void executeBeforeInput() throw(DomeException) {};
	void execute() throw(DomeException);
	void executeAfterOutput() throw(DomeException) {};

	void displayDataNames ();

	//test method
	vector< vector<int> > testIntMatrix(int** mat, int rows, int cols){
		vector< vector<int> > ret(rows);
		for(int i =0; i < rows; i++) {
			vector<int> temp(cols);
			for(int j =0; j< cols; j++) {
				temp[j] = mat[i][j];
			}
			ret[i] = temp;
		}
		return ret;
	};

	
	vector <MatlabData*> getData ()
	{
		return m_data;
	}


private:
	#define _OUTPUT_BUFFER_SIZE	5000
	char m_buffer[_OUTPUT_BUFFER_SIZE+1];
	char commandLine[_OUTPUT_BUFFER_SIZE+1]; // engevalString expects for loops in one line

	string m_filename;
	bool m_isVisible;
	bool m_isExecutable;
	vector <MatlabData*> m_data;

	Engine *m_engine;					// Pointer to the MATLAB engine
	vector <string> m_commands; 

	void createConnections() throw(DomeException);
	void destroyConnections() throw(DomeException);
	void wait( int seconds );
};

} // namespace MatlabPlugin
} // DOME

#endif // DOME_MATLABMODEL_H
