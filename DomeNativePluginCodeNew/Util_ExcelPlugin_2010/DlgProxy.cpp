
// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "Util_ExcelPlugin_2010.h"
#include "DlgProxy.h"
#include "Util_ExcelPlugin_2010Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUtil_ExcelPlugin_2010DlgAutoProxy

IMPLEMENT_DYNCREATE(CUtil_ExcelPlugin_2010DlgAutoProxy, CCmdTarget)

CUtil_ExcelPlugin_2010DlgAutoProxy::CUtil_ExcelPlugin_2010DlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CUtil_ExcelPlugin_2010Dlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CUtil_ExcelPlugin_2010Dlg)))
		{
			m_pDialog = reinterpret_cast<CUtil_ExcelPlugin_2010Dlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CUtil_ExcelPlugin_2010DlgAutoProxy::~CUtil_ExcelPlugin_2010DlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CUtil_ExcelPlugin_2010DlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CUtil_ExcelPlugin_2010DlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CUtil_ExcelPlugin_2010DlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Note: we add support for IID_IUtil_ExcelPlugin_2010 to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {F88ACF61-481B-4843-9DD1-22267D6865BA}
static const IID IID_IUtil_ExcelPlugin_2010 =
{ 0xF88ACF61, 0x481B, 0x4843, { 0x9D, 0xD1, 0x22, 0x26, 0x7D, 0x68, 0x65, 0xBA } };

BEGIN_INTERFACE_MAP(CUtil_ExcelPlugin_2010DlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CUtil_ExcelPlugin_2010DlgAutoProxy, IID_IUtil_ExcelPlugin_2010, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {9BE399C3-E55B-48E1-8302-8491287C72E9}
IMPLEMENT_OLECREATE2(CUtil_ExcelPlugin_2010DlgAutoProxy, "Util_ExcelPlugin_2010.Application", 0x9be399c3, 0xe55b, 0x48e1, 0x83, 0x2, 0x84, 0x91, 0x28, 0x7c, 0x72, 0xe9)


// CUtil_ExcelPlugin_2010DlgAutoProxy message handlers
