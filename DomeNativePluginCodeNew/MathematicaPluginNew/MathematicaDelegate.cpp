//----------------------------------------------------------------------------
// MathematicaDelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <vector>
#include <string>
#include <iostream>
using namespace std;

// java native interface stuff
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include "JNIHelper.h"

// Mathematica function prototypes
#include "MathematicaIncludes.h"

// data and model classes
#include "MathematicaData.h"
#include "MathematicaModel.h"
using namespace DOME::MathematicaPlugin;

// definitions for this class
#include "MathematicaPluginCaller.h"
#include "MathematicaDelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long MathematicaDelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case MathematicaPluginCaller_MODEL_CREATE_REAL:
	case MathematicaPluginCaller_MODEL_CREATE_INTEGER:
		iObjectPtr = (long) createModelObject (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case MathematicaPluginCaller_MODEL_CREATE_VECTOR:
		iObjectPtr = (long) createVector (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_MODEL_CREATE_MATRIX:
		iObjectPtr = (long) createMatrix (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void MathematicaDelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MODEL_LOAD:
		loadModel (iObjectPtr);
		break;

	case MathematicaPluginCaller_MODEL_EXECUTE:
		executeModel (iObjectPtr);
		break;

	case MathematicaPluginCaller_MODEL_EXEC_BF_INPUT:
		break;

	case MathematicaPluginCaller_MODEL_EXEC_AF_INPUT:
		break;

	case MathematicaPluginCaller_MODEL_UNLOAD:
		unloadModel (iObjectPtr);
		break;

	case MathematicaPluginCaller_MODEL_DESTROY:
		destroyModel (iObjectPtr);
		break;

	case MathematicaPluginCaller_REAL_SET_VALUE:
		setRealValue (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_INTEGER_SET_VALUE:
		setIntegerValue (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_VECTOR_SET_ELEMENT:
		setVectorElement (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_VECTOR_SET_VALUES:
		setVectorValues (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_MATRIX_SET_ELEMENT:
		setMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_MATRIX_SET_VALUES:
		setMatrixValues (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_MATRIX_SET_ROWS:
	case MathematicaPluginCaller_MATRIX_SET_COLS:
		setMatrixDimension (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case MathematicaPluginCaller_MATRIX_SET_DIM:
		setMatrixDimensions (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int MathematicaDelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MATRIX_GET_ROWS:
	case MathematicaPluginCaller_MATRIX_GET_COLS:
	case MathematicaPluginCaller_VECTOR_GET_LENGTH:
	case MathematicaPluginCaller_INTEGER_GET_VALUE:
		value = getIntegerValue (iObjectPtr, iFunctionIndex);
		break;

	default:
		MATHEMATICA_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool MathematicaDelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (iObjectPtr);
		break;
		
	default:
		MATHEMATICA_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double MathematicaDelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into MathematicaDelegate::callDoubleFunctions!" << endl;
	//cout << "iFunctionIndex = " << iFunctionIndex << ", MathematicaPluginCaller_REAL_GET_VALUE = "<< MathematicaPluginCaller_REAL_GET_VALUE << endl;
	
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_REAL_GET_VALUE:
		value = getDoubleValue (iObjectPtr);
		break;

	case MathematicaPluginCaller_MATRIX_GET_ELEMENT:
		value = getMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	case MathematicaPluginCaller_VECTOR_GET_ELEMENT:
		value = getVectorElement (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const
char* MathematicaDelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char *value = NULL;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATHEMATICA_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
MathematicaDelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_VECTOR_GET_VALUES:
		result = getDoubleArrayValue (iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
MathematicaDelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MATRIX_GET_DIM:
		result = getIntArrayValue (iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
MathematicaDelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		MATHEMATICA_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
MathematicaDelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case MathematicaPluginCaller_MATRIX_GET_VALUES:
		result = getMatrixElements (iObjectPtr);
		break;

	default:
		MATHEMATICA_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void MathematicaDelegate::loadModel (long iObjectPtr)
{
	MathematicaModel *model = (MathematicaModel*) iObjectPtr;

	if (model == NULL) {
		MATHEMATICA_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void MathematicaDelegate::unloadModel (long iObjectPtr)
{
	MathematicaModel *model = (MathematicaModel*) iObjectPtr;

	if (model == NULL) {
		MATHEMATICA_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->unloadModel ();
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void MathematicaDelegate::destroyModel (long iObjectPtr)
{
	MathematicaModel *model = (MathematicaModel*) iObjectPtr;

	if (model == NULL) {
		MATHEMATICA_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}


/////////////////////////////////////////////////////
// set real values
/////////////////////////////////////////////////////
void MathematicaDelegate::setRealValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setRealValue function");
	}
	else 
	{
		//MathematicaReal *parameter = (MathematicaReal*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;
		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setRealValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_DOUBLE, value);
			if (err != ERR_OK) {
				MATHEMATICA_DEBUG ("setRealValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				//cout << "setRealValue: " << *((double*)value) << endl;
				((MathematicaReal*)parameter)->setValue ( *((double*)value) );
			}

			// clean up
			if (value != NULL) delete (double*) value;
		}
	}
}

/////////////////////////////////////////////////////
// set integer values
/////////////////////////////////////////////////////
void MathematicaDelegate::setIntegerValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setIntegerValue function");
	}
	else 
	{
		//MathematicaReal *parameter = (MathematicaReal*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;
		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setIntegerValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_INTEGER, value);
			if (err != ERR_OK) {
				MATHEMATICA_DEBUG ("setIntegerValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				//cout << "setRealValue: " << *((double*)value) << endl;
				((MathematicaInteger*)parameter)->setValue ( *((int*)value) );
			}

			// clean up
			if (value != NULL) delete (int*) value;
		}
	}
}

/////////////////////////////////////////////////////
// set vector element
/////////////////////////////////////////////////////
void MathematicaDelegate::setVectorElement (JNIEnv* env, jobjectArray args,
									   unsigned int iNumArgs,
									   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 2) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setVectorElement function");
	}
	else 
	{
		int err1, err2;
		void *row = NULL;
		void *value = NULL;


		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_DOUBLE, value);

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setVectorElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("setVectorElement: Invalid row "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("setVectorElement: Invalid parameter value "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else {
			((MathematicaVector*)parameter)->setElement (*((int*)row),*((double*)value) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (value != NULL) delete (double*) value;
	}
}

/////////////////////////////////////////////////////
// set vector values
/////////////////////////////////////////////////////
void MathematicaDelegate::setVectorValues (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs,
									  long iObjectPtr)
{

	//for debug
	//cout<<"Get into MathematicaDelegate::setVectorValues!" << endl;
	
	// check argument list length
	if (iNumArgs != 2) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setVectorValues function");
	}
	else 
	{
		int err1, err2;
		void *iNumElements = NULL;
		void *values = NULL;


		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		//*****iObjectPtr should be cast to MathematicaData first since it has been sent to dome as MathematicaData
		//*****otherwise, there will be a problem
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_INTEGER, iNumElements);
		err2 = getArgument (env, args, 1, JAVA_1D_DOUBLE_ARRAY, values);

		//for debug
		//cout<<"MathematicaDelegate::setMatrixValues unpack parameters!" << endl;

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setVectorValues: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("setVectorValues: Invalid number of rows "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("setVectorValues: Invalid parameter value "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else {
			int iCols = *((int*)iNumElements);

			vector <double> vec(iCols);

			double* dValues = (double*)values;

			for (int i = 0; i < iCols; i++) {
					vec[i] = dValues[i];
			}


			// set vector values
			((MathematicaVector*)parameter)->setValues (vec); 

		}

		// clean up
		if (values != NULL) delete (double*) values;
		if (iNumElements != NULL) delete (int*) iNumElements;
	}
}



/////////////////////////////////////////////////////
// set matrix element
/////////////////////////////////////////////////////
void MathematicaDelegate::setMatrixElement (JNIEnv* env, jobjectArray args,
									   unsigned int iNumArgs,
									   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 3) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixElement function");
	}
	else 
	{
		int err1, err2, err3;
		void *row = NULL;
		void *col = NULL;
		void *value = NULL;


		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);
		err3 = getArgument (env, args, 2, JAVA_DOUBLE, value);

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid row "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid column "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid parameter value "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			((MathematicaMatrix*)parameter)->setElement (*((int*)row), 
								   *((int*)col),
								   *((double*)value) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
		if (value != NULL) delete (double*) value;
	}
}


/////////////////////////////////////////////////////
// set matrix values
/////////////////////////////////////////////////////
void MathematicaDelegate::setMatrixValues (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs,
									  long iObjectPtr)
{

	//for debug
	//cout<<"Get into MathematicaDelegate::setMatrixValues!" << endl;
	
	// check argument list length
	if (iNumArgs != 3) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixValues function");
	}
	else 
	{
		int err1, err2, err3;
		void *rows = NULL;
		void *cols = NULL;
		void *values = NULL;


		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		//*****iObjectPtr should be cast to MathematicaData first since it has been sent to dome as MathematicaData
		//*****otherwise, there will be a problem
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_2D_DOUBLE_ARRAY, values);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, cols);

		//for debug
		//cout<<"MathematicaDelegate::setMatrixValues unpack parameters!" << endl;

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setMatrixValues: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixValues: Invalid parameter value "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixValues: Invalid number of rows "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixValues: Invalid number of columns "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			// create matrix
			int iCols = *((int*)cols);
			int iRows = *((int*)rows);

			//for debug
			//cout<< "Rows: " << iRows << ", Cols:" << iCols << endl;

			double** dValues = (double**)values;

			//for debug
			//cout<< "Pointer of values: " << values  << endl;

			//*****Two ways to create: vector< vector<double> > mat
			//Way one:
/*			vector< vector<double> > mat;
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> row;
				for (int j = 0; j < iCols; j++) {
					//for debug
					cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;

					row.push_back(dValues[i][j]);
				}
				mat.push_back(row);
			}
*/
			//Way two:
			vector <vector <double> > mat(iRows);
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> vec(iCols);
				for (int j = 0; j < iCols; j++) {
					vec[j] = dValues[i][j];
					
					//for debug
					//cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;
				}	
				mat[i] = vec;
			}

			//for debug
			//cout<<"MathematicaDelegate::setMatrixValues is done!" << endl;

			// set matrix values
			((MathematicaMatrix*)parameter)->setValues (mat); //There is a problem to pass STL class as an argument

		}

		// clean up
		if (values != NULL) delete (double**) values;
		if (rows != NULL) delete (int*) rows;
		if (cols != NULL) delete (int*) cols;
	}
}



/////////////////////////////////////////////////////
// set number of rows or columns for a matrix
/////////////////////////////////////////////////////
void MathematicaDelegate::setMatrixDimension (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	// check argument list length
	if (iNumArgs != 1) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimension function");
	}
	else 
	{
		int err;
		void *dim = NULL;

		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_INTEGER, dim);

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid dimension "
						 << "(ERR = " << err << ")" << endl);
		}
		else {
			switch (iFunctionIndex)
			{
			case MathematicaPluginCaller_MATRIX_SET_COLS:
				((MathematicaMatrix*)parameter)->setColumns ( *((int*)dim) );
				break;

			case MathematicaPluginCaller_MATRIX_SET_ROWS:
				((MathematicaMatrix*)parameter)->setRows ( *((int*)dim) );
				break;

			default:
				MATHEMATICA_DEBUG ("setMatrixDimension: Undefined function index ("
							  << iFunctionIndex << ")" << endl);
			}
		}

		// clean up
		if (dim != NULL) delete (int*) dim;
	}
}


/////////////////////////////////////////////////////
// set matrix dimensions
/////////////////////////////////////////////////////
void MathematicaDelegate::setMatrixDimensions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 2) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimensions function");
	}
	else 
	{
		int err1, err2;
		void *rows = NULL;
		void *cols = NULL;


		// unpack the argument and call the function
		//MathematicaMatrix *parameter = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *parameter = (MathematicaData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, rows);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, cols);

		if (parameter == NULL) {
			MATHEMATICA_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid number of rows "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("setMatrixElement: Invalid number of columns "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else {
			((MathematicaMatrix*)parameter)->setDimension (*((int*)rows),
									 *((int*)cols));
		}

		// clean up
		if (cols != NULL) delete (int*) cols;
		if (rows != NULL) delete (int*) rows;
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
MathematicaModel *MathematicaDelegate::initializeModel (JNIEnv* env, jobjectArray args,
											  unsigned int iNumArgs)
{
	MathematicaModel *model = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2, err3;
		void* szFileName = NULL;
		void* bIsVisible = NULL;
		void* bIsExecutable = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szFileName);
		err2 = getArgument (env, args, 1, JAVA_BOOLEAN, bIsVisible);
		err3 = getArgument (env, args, 2, JAVA_BOOLEAN, bIsExecutable);

		//cout << "finish unpacking the arguments!" << endl;

		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("initializeModel: Invalid model file name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("initializeModel: Invalid argument: bIsVisible (ERR = " 
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATHEMATICA_DEBUG ("initializeModel: Invalid argument: bIsExecutable (ERR = " 
						 << err3 << ")" << endl);
		}
		else {
			// call the constructor
			//cout << "ready to call model constructor!" << endl;
			//cout << "file name: " << (char*)szFileName << endl;
			model = new MathematicaModel ((char*)szFileName);
		}

		// clean up
		//cout << "clean up!" << endl;
		if (szFileName != NULL) delete (char*) szFileName;
		if (bIsVisible != NULL)	delete (bool*) bIsVisible;
		if (bIsExecutable != NULL)	delete (bool*) bIsExecutable;
	}

	return model;
}


///////////////////////////////////////////
// create individual model objects
///////////////////////////////////////////
MathematicaData *MathematicaDelegate::createModelObject (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr,
												unsigned int iFunctionIndex)
{
	MathematicaData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createModelObject function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		MathematicaModel *model = (MathematicaModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			MATHEMATICA_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			MATHEMATICA_DEBUG ("createModelObject: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			switch (iFunctionIndex)
			{
			case MathematicaPluginCaller_MODEL_CREATE_REAL:
				modelObject = model->createReal (name);
				break;

			case MathematicaPluginCaller_MODEL_CREATE_INTEGER:
				modelObject = model->createInteger (name);
				break;

			default:
				MATHEMATICA_DEBUG ("createModelObject: Undefined function index ("
							  << iFunctionIndex << ")" << endl);
			}

		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}

///////////////////////////////////////////
// create vector
///////////////////////////////////////////
MathematicaData *MathematicaDelegate::createVector (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	MathematicaData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createVector function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szParameterName = NULL;
		void* rows = NULL;

		// unpack the arguments
		MathematicaModel *model = (MathematicaModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);

		if (model == NULL) {
			MATHEMATICA_DEBUG ("createVector: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("createVector: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("createVector: Invalid number of cols (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createVector ((char*)szParameterName, *((int*)rows));
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
		if (rows != NULL) delete (int*) rows;
	}

	return modelObject;
}



///////////////////////////////////////////
// create matrix
///////////////////////////////////////////
MathematicaData *MathematicaDelegate::createMatrix (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	MathematicaData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createMatrix function" << endl);
	}
	else 
	{
		int err1, err2, err3;
		void* szParameterName = NULL;
		void* rows = NULL;
		void* cols = NULL;

		// unpack the arguments
		MathematicaModel *model = (MathematicaModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, cols);

		if (model == NULL) {
			MATHEMATICA_DEBUG ("createMatrix: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("createMatrix: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("createMatrix: Invalid number of rows (ERR = "
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			MATHEMATICA_DEBUG ("createMatrix: Invalid number of columns (ERR = "
						 << err3 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createMatrix ((char*)szParameterName,
											   *((int*)rows),
											   *((int*)cols));
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
		if (rows != NULL) delete (int*) rows;
		if (cols != NULL) delete (int*) cols;
	}

	return modelObject;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
double MathematicaDelegate::getDoubleValue (long iObjectPtr)
{
	//for debug
	//cout << "Get into MathematicaDelegate::getDoubleValue" << endl;

	double value = 0;
	//MathematicaReal *data = (MathematicaReal*) iObjectPtr;
	MathematicaData *data = (MathematicaData*) iObjectPtr;

	if (data == NULL) {
		MATHEMATICA_DEBUG ("getDoubleValue: MathematicaReal does not exist" << endl);
	}
	else {
		// get the value
		value = ((MathematicaReal*)data)->getValue ();
	}

	return value;
}


///////////////////////////////////////////
// get element from matrix
///////////////////////////////////////////
double MathematicaDelegate::getMatrixElement (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	double value = 0;

	// check argument list length
	if (iNumArgs != 2) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the getMatrixElement function" << endl);
	}
	else 
	{
		int err1, err2;
		void* row = NULL;
		void* col = NULL;

		// unpack the arguments
		//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *data = (MathematicaData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);

		if (data == NULL) {
			MATHEMATICA_DEBUG ("getMatrixElement: MathematicaMatrix does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("getMatrixElement: Invalid row (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			MATHEMATICA_DEBUG ("getMatrixElement: Invalid column (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			value = ((MathematicaMatrix*)data)->getElement (*((int*)row),
									  *((int*)col) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
	}

	return value;
}

///////////////////////////////////////////
// get element from vector
///////////////////////////////////////////
double MathematicaDelegate::getVectorElement (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	double value = 0;

	// check argument list length
	if (iNumArgs != 1) {
		MATHEMATICA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the getVectorElement function" << endl);
	}
	else 
	{
		int err1;
		void* row = NULL;

		// unpack the arguments
		//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
		MathematicaData *data = (MathematicaData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);

		if (data == NULL) {
			MATHEMATICA_DEBUG ("getVectorElement: MathematicaMatrix does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			MATHEMATICA_DEBUG ("getVectorElement: Invalid row (ERR = "
						 << err1 << ")" << endl);
		}
		else
		{
			// call the function
			value = ((MathematicaVector*)data)->getElement (*((int*)row));
		}

		// clean up
		if (row != NULL) delete (int*) row;
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get an integer value
///////////////////////////////////////////
int MathematicaDelegate::getIntegerValue (long iObjectPtr,
									 unsigned int iFunctionIndex)
{
	int value = 0;

	//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
	MathematicaData *data = (MathematicaData*) iObjectPtr;

	if (data == NULL) {
		MATHEMATICA_DEBUG ("getIntegerValue: MathematicaMatrix does not exist" << endl);
	}
	else {
		switch (iFunctionIndex)
		{
		case MathematicaPluginCaller_MATRIX_GET_ROWS:
			value = ((MathematicaMatrix*)data)->getRows ();
			break;

		case MathematicaPluginCaller_MATRIX_GET_COLS:
			value = ((MathematicaMatrix*)data)->getColumns ();
			break;

		case MathematicaPluginCaller_VECTOR_GET_LENGTH:
			value = ((MathematicaVector*)data)->getLength ();
			break;

		case MathematicaPluginCaller_INTEGER_GET_VALUE:
			value = ((MathematicaInteger*)data)->getValue ();
			break;


		default:
			MATHEMATICA_DEBUG ("getIntegerValue: Undefined function index ("
						  << iFunctionIndex << ")" << endl);
		}
	}

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
void MathematicaDelegate::executeModel (long iObjectPtr)
{
	MathematicaModel *model = (MathematicaModel*) iObjectPtr;

	if (model == NULL) {
		MATHEMATICA_DEBUG ("executeModel: MathematicaModel does not exist" << endl);
	}
	else {
		// execute the model
		model->execute ();
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool MathematicaDelegate::getBooleanValue (long iObjectPtr)
{
	bool value = false;
	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool MathematicaDelegate::isModelLoaded (long iObjectPtr)
{
	bool value = false;
	MathematicaModel *model = (MathematicaModel*) iObjectPtr;

	if (model == NULL) {
		MATHEMATICA_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a string parameter
///////////////////////////////////////////
const char* MathematicaDelegate::getStringValue (long iObjectPtr)
{
	const char* value = NULL;
	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 1D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the dimension from a matrix parameter
/////////////////////////////////////////////////
vector<int> 
MathematicaDelegate::getIntArrayValue (long iObjectPtr)
{
	vector<int> value;

	// unpack the arguments
	//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
	MathematicaData *data = (MathematicaData*) iObjectPtr;

	if (data == NULL) {
		MATHEMATICA_DEBUG ("getIntArrayValue: MathematicaMatrix does not exist" << endl);
	}
	else
	{
		// call the function
		value = ((MathematicaMatrix*)data)->getDimension ();
	}

	return value;
}



/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////
vector<double> 
MathematicaDelegate::getDoubleArrayValue (long iObjectPtr)
{
	vector<double> value;

	// unpack the arguments
	//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
	MathematicaData *data = (MathematicaData*) iObjectPtr;

	if (data == NULL) {
		MATHEMATICA_DEBUG ("getDoubleArrayValue: MathematicaVector does not exist" << endl);
	}
	else
	{
		// call the function
		value = ((MathematicaVector*)data)->getValues ();
	}

	return value;
}




//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 2D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//////////////////////////////////////////////////////
// get the values from a 2D double array parameter
//////////////////////////////////////////////////////
vector< vector<double> > 
MathematicaDelegate::getMatrixElements (long iObjectPtr)
{
	vector< vector<double> > value;

	// unpack the arguments
	//MathematicaMatrix *data = (MathematicaMatrix*) iObjectPtr;
	MathematicaData *data = (MathematicaData*) iObjectPtr;

	if (data == NULL) {
		MATHEMATICA_DEBUG ("getMatrixElements: MathematicaMatrix does not exist" << endl);
	}
	else
	{
		// call the function
		value = ((MathematicaMatrix*)data)->getValues ();
	}

	return value;
}
