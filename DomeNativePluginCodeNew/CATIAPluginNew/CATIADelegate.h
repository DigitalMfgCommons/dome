//----------------------------------------------------------------------------
// CATIADelegate.h
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********          (EXCEPT WHERE NOTED) AS NEEDED           ************
//----------------------------------------------------------------------------


#ifndef DOME_CATIADelegate_H
#define DOME_CATIADelegate_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// forward definitions for files that include this file
#ifndef DELEGATE_CPP
class CATIAModel;
class CATIAData;
class CATIAFile;
class CATIAVector;
class CATIAString;
#endif


class CATIADelegate
{
public:
	CATIADelegate () {}


	//////////////////////////////////////////////////////////////////////////////////////
	// entry points to this class
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (begin) ************

	void     callVoidFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								long iObjectPtr, unsigned int iFunctionIndex);

	int      callIntegerFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	double	 callDoubleFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	bool     callBooleanFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	const
	char*    callStringFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	long     callObjectFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector<int>
		callIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	vector<double>
		callDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<int> >
		call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									 long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<double> > 
		call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									long iObjectPtr, unsigned int iFunctionIndex);

	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (end) ************
	//////////////////////////////////////////////////////////////////////////////////////





	//////////////////////////////////////////////////////////////////////////////////////
	// object functions
	//////////////////////////////////////////////////////////////////////////////////////

	CATIAModel  *initializeModel (JNIEnv* env, jobjectArray args, unsigned int iNumArgs);

	CATIAData   *createModelObject (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								    long iObjectPtr, unsigned int iFunctionIndex);

	CATIAFile   *createFileObject (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr);

	CATIAData *createUserLibraryModelObject (JNIEnv* env, jobjectArray args, 
											 unsigned int iNumArgs, 
											 long iObjectPtr,
											 unsigned int iFunctionIndex);

	CATIAVector *createUserLibraryVectorObject (JNIEnv* env, jobjectArray args, 
												unsigned int iNumArgs, 
												long iObjectPtr);


	//////////////////////////////////////////////////////////////////////////////////////
	// void functions
	//////////////////////////////////////////////////////////////////////////////////////

	void	loadModel (JNIEnv* env, long iObjectPtr);
	void	unloadModel (JNIEnv* env, jobjectArray args, unsigned int iNumArgs, long iObjectPtr);
	void	destroyModel (JNIEnv* env, long iObjectPtr);
	void	setupUserLibrary (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
							  long iObjectPtr);
	void	setValue (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
					  long iObjectPtr, unsigned int iFunctionIndex);
	void	set1DArrayValues (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
							  long iObjectPtr, unsigned int iFunctionIndex);


	//////////////////////////////////////////////////////////////////////////////////////
	// real functions
	//////////////////////////////////////////////////////////////////////////////////////

	double	getDoubleValue (JNIEnv* env, long iObjectPtr);
	

	//////////////////////////////////////////////////////////////////////////////////////
	// integer functions
	//////////////////////////////////////////////////////////////////////////////////////

	int		executeModel (JNIEnv* env, long iObjectPtr);
	int		getIntegerValue (JNIEnv* env, long iObjectPtr);


	//////////////////////////////////////////////////////////////////////////////////////
	// boolean functions
	//////////////////////////////////////////////////////////////////////////////////////

	bool	getBooleanValue (JNIEnv* env, long iObjectPtr);
	bool	isModelLoaded (JNIEnv* env, long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// string functions
	//////////////////////////////////////////////////////////////////////////////////////

	const
	char*	getStringValue (JNIEnv* env, long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// array functions
	//////////////////////////////////////////////////////////////////////////////////////

	vector<double> 
			getDoubleArrayValue (JNIEnv* env, long iObjectPtr);

};


#endif // DOME_CATIADelegate_H
