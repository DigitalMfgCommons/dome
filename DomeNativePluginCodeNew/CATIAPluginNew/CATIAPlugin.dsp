# Microsoft Developer Studio Project File - Name="CATIAPlugin" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=CATIAPlugin - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CATIAPlugin.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CATIAPlugin.mak" CFG="CATIAPlugin - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CATIAPlugin - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "CATIAPlugin - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CATIAPlugin - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CATIAPLUGIN_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "C:\DS\B13\ObjectModelerBase\ProtectedInterfaces" /I "C:\DOME\CADPlugInsLocal\CATIAPlugin\CatiaHeaders" /I "C:\DS\B13\System\ProtectedInterfaces" /I "C:\DS\B13\System\PublicInterfaces" /I "C:\DS\B13\ObjectSpecsModeler\ProtectedInterfaces" /I "C:\DS\B13\NewTopologicalObjects\ProtectedInterfaces" /I "C:\DS\B13\GeometricObjects\ProtectedInterfaces" /I "C:\DS\B13\Mathematics\PublicInterfaces" /I "C:\DS\B13\Mathematics\ProtectedInterfaces" /I "C:\DS\B13\GeometricObjects\PublicInterfaces" /I "C:\DS\B13\SpecialAPI\ProtectedInterfaces" /I "C:\DS\B13\NewTopologicalObjects\PublicInterfaces" /I "C:\DS\B13\MecModInterfaces\ProtectedInterfaces" /I "C:\DS\B13\MechanicalModeler\ProtectedInterfaces" /I "C:\DS\B13\LiteralFeatures\ProtectedInterfaces" /I "C:\DS\B13\MechanicalModelerUI\ProtectedInterfaces" /I "C:\DS\B13\CATIAApplicationFrame\ProtectedInterfaces" /I "C:\DS\B13\Visualization\ProtectedInterfaces" /I "C:\DS\B13\VisualizationBase\ProtectedInterfaces" /I "C:\DS\B13\KnowledgeInterfaces\ProtectedInterfaces" /I "C:\DS\B13\PartInterfaces\ProtectedInterfaces" /I "C:\DS\B13\SpecialAPI\PublicInterfaces" /I "C:\DS\B13\CAADoc\CAAVisualization.edu\CAAVisBasicAppli.m\src" /I "C:\DS\B13\ObjectModelerBase\PublicInterfaces" /I "C:\DS\B13\ObjectSpecsModeler\PublicInterfaces" /I "C:\DS\B13\MecModInterfaces\PublicInterfaces" /I "C:\DS\B13\MechanicalModeler\PublicInterfaces" /I "C:\DS\B13\KnowledgeInterfaces\PublicInterfaces" /I "C:\DS\B13\MechanicalModelerUI\PublicInterfaces" /I "C:\DS\B13\CATIAApplicationFrame\PublicInterfaces" /I "C:\DS\B13\Visualization\PublicInterfaces" /I "C:\DS\B13\VisualizationBase\PublicInterfaces" /I "C:\DS\B13\PartInterfaces\PublicInterfaces" /I "C:\DS\B13\InfInterfaces\PublicGenerated\intel_a" /I "C:\DS\B13\MecModInterfaces\PublicGenerated\intel_a" /I "C:\DS\B13\KnowledgeInterfaces\PublicGenerated\intel_a" /I "C:\dome3\development\nativeplugincode\CATIADomeInterfaces\CATIADomeInterfaces\PublicGenerated\intel_a" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CATIAPLUGIN_EXPORTS" /D "_WINDOWS_SOURCE" /D "_WINDLL" /D "_AFXDLL" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 JS0GROUP.lib CATMecModInterfaces.lib CATObjectModelerBase.lib CATObjectSpecsModeler.lib KnowledgeItf.lib CATViz.lib CATVisualization.lib DI0PANV2.lib JS0FM.lib CATMathematics.lib CATMechanicalModeler.lib CATApplicationFrame.lib CATInfInterfaces.lib CATIADomeItf.lib /nologo /dll /debug /machine:I386 /libpath:"C:\DS\B13\intel_a\code\lib" /libpath:"C:\dome3\development\nativeplugincode\CATIADomeInterfaces\intel_a\code\lib"

!ELSEIF  "$(CFG)" == "CATIAPlugin - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CATIAPLUGIN_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "C:\Program Files\Dassault Systemes\B13\ObjectModelerBase\ProtectedInterfaces" /I "C:\DOME\CADPlugInsLocal\CATIAPlugin\CatiaHeaders" /I "C:\Program Files\Dassault Systemes\B13\System\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\System\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\ObjectSpecsModeler\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\NewTopologicalObjects\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\GeometricObjects\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\Mathematics\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\Mathematics\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\GeometricObjects\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\SpecialAPI\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\NewTopologicalObjects\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MecModInterfaces\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MechanicalModeler\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\LiteralFeatures\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MechanicalModelerUI\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\CATIAApplicationFrame\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\Visualization\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\VisualizationBase\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\KnowledgeInterfaces\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\PartInterfaces\ProtectedInterfaces" /I "C:\Program Files\Dassault Systemes\B13\SpecialAPI\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\CAADoc\CAAVisualization.edu\CAAVisBasicAppli.m\src" /I "C:\Program Files\Dassault Systemes\B13\ObjectModelerBase\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\ObjectSpecsModeler\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MecModInterfaces\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MechanicalModeler\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\KnowledgeInterfaces\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\MechanicalModelerUI\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\CATIAApplicationFrame\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\Visualization\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\VisualizationBase\PublicInterfaces" /I "C:\Program Files\Dassault Systemes\B13\PartInterfaces\PublicInterfaces" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CATIAPLUGIN_EXPORTS" /D "_WINDOWS_SOURCE" /D "_WINDLL" /D "_AFXDLL" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 JS0GROUP.lib CATMecModInterfaces.lib CATObjectModelerBase.lib CATObjectSpecsModeler.lib KnowledgeItf.lib CATViz.lib CATVisualization.lib DI0PANV2.lib JS0FM.lib CATMathematics.lib CATMechanicalModeler.lib /nologo /dll /debug /machine:I386 /pdbtype:sept /libpath:"C:\Program Files\Dassault Systemes\B13\intel_a\code\lib"

!ENDIF 

# Begin Target

# Name "CATIAPlugin - Win32 Release"
# Name "CATIAPlugin - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CatiaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CATIADelegate.cpp
# End Source File
# Begin Source File

SOURCE=.\CatiaModel.cpp
# End Source File
# Begin Source File

SOURCE=.\CATIAPluginCaller.cpp
# End Source File
# Begin Source File

SOURCE=.\JNIHelper.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CatiaData.h
# End Source File
# Begin Source File

SOURCE=.\CATIADelegate.h
# End Source File
# Begin Source File

SOURCE=.\CatiaIncludes.h
# End Source File
# Begin Source File

SOURCE=.\CatiaModel.h
# End Source File
# Begin Source File

SOURCE=.\CATIAPluginCaller.h
# End Source File
# Begin Source File

SOURCE=.\JNIHelper.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\DebugLog.rc
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# End Group
# End Target
# End Project
