// MatlabIncludes.h: interface for the MatlabIncludes class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATLABINCLUDES_H
#define DOME_MATLABINCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DomePlugin.h"
using namespace DOME::DomePlugin;

#include "TypeConversions.h" // str
using namespace DOME::Utilities::TypeConversions;

#define MATLAB_DEBUG(msg) cout << "MATLAB DEBUG: " << msg << endl;
//#define MATLAB_ERROR1(msg) throw DomeException("Matlab Error",msg)
#define MATLAB_ERROR2(msg)		\
	{							\
		cout << msg << endl;	\
        throw DomeException("Matlab Error",__FILE__,__LINE__,msg);	\
	}
						   
#define MATLAB_ERROR MATLAB_ERROR2

#ifdef _MATLAB_R14
#include "R14_matlabinclude/engine.h"
#endif

#ifdef _MATLAB_R13
#include "R13_matlabinclude/engine.h"
#endif

#ifdef _MATLAB_R12
#include "R12_matlabinclude/engine.h"
#endif

#ifdef _MATLAB
#include "engine.h"
#endif

#endif // DOME_MATLABINCLUDES_H
