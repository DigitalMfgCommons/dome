//--------------------------------------------------------------------------
// JNIHelper.H
//
// Purpose: retrieve an argument from a list of Java-wrapped parameters
//
//
// *********** NO MODIFICATION NECESSARY BY THE PLUGIN AUTHOR ************
//--------------------------------------------------------------------------

#ifndef _JNI_HELPER_H
#define _JNI_HELPER_H

enum ERR_CODES {
	ERR_OK						=  0,
	ERR_INVALID_JAVA_OBJECT		= -1,
	ERR_INVALID_ARGUMENT_NUMBER = -2,
	ERR_INVALID_TYPE			= -3,
	ERR_INVALID_JAVA_CALL		= -4,
	ERR_JNI_INTERNAL			= -5
};

enum JAVA_DATA_TYPES {
	JAVA_STRING,
	JAVA_BOOLEAN,
	JAVA_DOUBLE,
	JAVA_INTEGER,
	JAVA_1D_INTEGER_ARRAY,
	JAVA_1D_DOUBLE_ARRAY,
	JAVA_2D_INTEGER_ARRAY,
	JAVA_2D_DOUBLE_ARRAY,
	NUM_JAVA_DATA_TYPES
};


int getArgument (JNIEnv* env, jobjectArray args, int iArgIndex, int iType, void *&value);


#endif
